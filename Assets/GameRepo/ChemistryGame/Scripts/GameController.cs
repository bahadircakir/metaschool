using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class GameController : MonoBehaviour
{
    [SerializeField]
    public GameObject []level1Tubes;
    public GameObject []level2Tubes;
    public GameObject []level3Tubes;
    public static GameController instance;
    public GameObject player;
    public GameObject[] levels;
    private Vector3 defaultPlayerLocation = new Vector3(0f,0f,15f);
    public bool checkForLevel1End;
    public bool checkForLevel2End;
    public bool checkForLevel3End;
    public SceneLoaderGame sceneLoaderGame;
    
    private Random random;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        random = new Random();

        level1Creater();
        level2Creater();
        level3Creater();
        
        AudioManager.instance.Stop("Menu_Background");
        AudioManager.instance.Play("Game_Background");
    }

    private void level1Creater()
    {
        float minZ = 15;
        var maxZ = minZ+level1Tubes[0].GetComponent<Collider>().bounds.size.z;
        for (var i = 0; i < 5; i++)
        {
            if (i != 0)
            {
                minZ = maxZ;
                maxZ += level1Tubes[i].GetComponent<Collider>().bounds.size.z;
            }
            var monsterNumber = random.Next(7, 10);
            MineralController.instance.CreateMonster(level1Tubes[i], monsterNumber, minZ, maxZ);
        }
    }
    
    private void level2Creater()
    {
        float minZ = 15;
        var maxZ = minZ+level2Tubes[0].GetComponent<Collider>().bounds.size.z;
        for (var i = 0; i < 4; i++)
        {
            if (i == 1 || i == 2)
            {
                minZ = maxZ;
                maxZ += level2Tubes[i].GetComponent<Collider>().bounds.size.z;
            }
            var monsterNumber = random.Next(10, 13);
            MineralController.instance.CreateMonster(level2Tubes[i], monsterNumber, minZ, maxZ);
        }
    }
    
    private void level3Creater()
    {
        float minZ = 15;
        var maxZ = minZ+level3Tubes[0].GetComponent<Collider>().bounds.size.z;
        for (var i = 0; i < 4; i++)
        {
            if (i == 1 || i == 2)
            {
                minZ = maxZ;
                maxZ += level3Tubes[i].GetComponent<Collider>().bounds.size.z;
            }
            var monsterNumber = random.Next(13, 16);
            MineralController.instance.CreateMonster(level3Tubes[i], monsterNumber, minZ, maxZ);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (levels[0].activeSelf && checkForLevel1End)
        {
            player.transform.position = defaultPlayerLocation;
            player.transform.rotation = Quaternion.identity;
            levels[0].SetActive(false);
            levels[1].SetActive(true);
        }
        else if (levels[1].activeSelf && checkForLevel2End)
        {
            player.transform.position = defaultPlayerLocation;
            player.transform.rotation = Quaternion.identity;
            levels[1].SetActive(false);
            levels[2].SetActive(true);
        }
        
        else if (levels[2].activeSelf && checkForLevel3End)
        {
            AudioManager.instance.Stop("Game_Background");
            AudioManager.instance.Play("Game_Win");
            sceneLoaderGame.EndGameScene();
        }
    }
}
