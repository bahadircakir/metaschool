﻿using System;
using UnityEngine;

public class WarmController : MonoBehaviour
{
    public float animationCompletionTime;
    public float range;
    
    private Vector3 initialPos;
    private Vector3 targetPos;
    private float timer;
    private bool animForwardBackward;

    private void Awake()
    {
        initialPos = transform.position;
        targetPos = initialPos + transform.forward * range;
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (animForwardBackward)
        {
            transform.position = initialPos + (targetPos - initialPos) * (timer / animationCompletionTime);
        }
        else
        {
            transform.position = initialPos + (targetPos - initialPos) * (1 - timer / animationCompletionTime);
        }
        if (timer >= animationCompletionTime)
        {
            timer = 0;
            animForwardBackward = !animForwardBackward;
        }
    }
    
}