using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoaderGameEnd : MonoBehaviour
{
    
    public void MenuButton()
    {
        StartCoroutine(BackToMainMenu());
    }
    
    public void RestartButton()
    {
        StartCoroutine(RestartGame());
    }

    public IEnumerator BackToMainMenu()
    {
        var operation = SceneManager.LoadSceneAsync("Main Menu");
        operation.allowSceneActivation = false;
        while (!operation.isDone) {
            if (operation.progress >= 0.9f)
            {
                operation.allowSceneActivation = true;
            }
            yield return null;
        }
    }
    public IEnumerator RestartGame()
    {
        var operation = SceneManager.LoadSceneAsync("Game", LoadSceneMode.Single);
        operation.allowSceneActivation = false;
        while (!operation.isDone) {
            if (operation.progress >= 0.9f)
            {
                operation.allowSceneActivation = true;
            }
            yield return null;
        }
    }
}
