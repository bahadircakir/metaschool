using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
public class WaterController : MonoBehaviour
{
    private Vector3[] spawnpoints;
    public GameObject gameobject;
    

    private void Awake()
    {
        spawnpoints = new Vector3[30];
    }

    void Start()
    {
        for (var i = 0; i < 30; i++)
        {
            spawnpoints[i] = new Vector3(0, 0, Random.Range(30, 1250));
            var centerPosition = spawnpoints[i];
            float radius = 16f;
            var ang = Random.Range(89,91);
            spawnpoints[i].x = (radius * Mathf.Cos(ang)) + centerPosition.x;
            spawnpoints[i].y = (radius * Mathf.Sin(ang)) + centerPosition.y;
            GameObject obj = Instantiate(gameobject, spawnpoints[i], Quaternion.identity);
            obj.transform.GetChild(0).GetComponent<Renderer>().enabled = false;
        }
    }
}
