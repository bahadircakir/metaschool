using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class MineralController : MonoBehaviour
{
    public static MineralController instance;
    private GameObject Player;
    private RaycastHit hit;
    
    public GameObject BorPrefab;
    public GameObject Co2Prefab;
    public GameObject H2oPrefab;
    public GameObject KursunPrefab;
    public GameObject O2Prefab;
    public GameObject SpongePrefab;
    public GameObject VirusPrefab;

    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }

    public void CreateMonster(GameObject tube, int monsterNumber, float minZ, float maxZ)
    {
        for (var i = 0; i < monsterNumber; i++)
        {
            var monsterType = Random.Range(0, 7);
            var zPoint = Random.Range((int)minZ, (int)maxZ);
            
            if (monsterType == 0)
                CreateBor(tube, zPoint);
            else if (monsterType == 1)
                CreateCO2(tube, zPoint);
            else if (monsterType == 2)
                CreateH2o(tube, zPoint);
            else if (monsterType == 3)
                CreateKursun(tube, zPoint);
            else if (monsterType == 4)
                CreateO2(tube, zPoint);
            else if (monsterType == 5)
                CreateSponge(tube, zPoint);
            else if (monsterType == 6)
                CreateVirus(tube, zPoint);
        }
        
        var z = Random.Range((int)minZ, (int)maxZ);
        Debug.Log(AntController.instance);
        AntController.instance.CreateAnt(tube, z);
        z = Random.Range((int)minZ, (int)maxZ);
        AntController.instance.CreateAnt(tube, z);
        z = Random.Range((int)minZ, (int)maxZ);
        AntController.instance.CreateAnt(tube, z);
    }

    public void CreateBor(GameObject tube, float zPoint)
    {
        var bounds = tube.GetComponent<Collider>().bounds;
        
        var ray = new Ray(new Vector3(bounds.center.x, bounds.center.y, zPoint), Vector3.left);
        Physics.Raycast(ray, out hit);

        var radius = Math.Abs(hit.point.x - bounds.center.x)*0.8f;
        var x = Random.Range(-(int)radius, (int)radius);
        var y = Random.Range(-(int)radius, (int)radius);

        var bor = Instantiate(BorPrefab);
        
        bor.transform.position = new Vector3(x, y, zPoint);
    }
    
    public void CreateCO2(GameObject tube, float zPoint)
    {

        var bounds = tube.GetComponent<Collider>().bounds;
        var ray = new Ray(new Vector3(bounds.center.x, bounds.center.y, zPoint), Vector3.left);
        Physics.Raycast(ray, out hit);
        var radius = Math.Abs(hit.point.x - bounds.center.x)*0.8f;
        
        var x = Random.Range(-(int)radius, (int)radius);
        var y = Random.Range(-(int)radius, (int)radius);
        
        var co2 = Instantiate(Co2Prefab);
        
        co2.transform.position = new Vector3(x, y, zPoint);
        
    }
    
    public void CreateH2o(GameObject tube, float zPoint)
    {
        var bounds = tube.GetComponent<Collider>().bounds;
        
        var ray = new Ray(new Vector3(bounds.center.x, bounds.center.y, zPoint), Vector3.left);
        Physics.Raycast(ray, out hit);

        var radius = Math.Abs(hit.point.x - bounds.center.x)*0.8f;
        var x = Random.Range(-(int)radius, (int)radius);
        var y = Random.Range(-(int)radius, (int)radius);

        var h2o = Instantiate(H2oPrefab);
        
        h2o.transform.position = new Vector3(x, y, zPoint);
    }
    
    public void CreateKursun(GameObject tube, float zPoint)
    {
        var bounds = tube.GetComponent<Collider>().bounds;
        
        var ray = new Ray(new Vector3(bounds.center.x, bounds.center.y, zPoint), Vector3.left);
        Physics.Raycast(ray, out hit);

        var radius = Math.Abs(hit.point.x - bounds.center.x)*0.8f;
        var x = Random.Range(-(int)radius, (int)radius);
        var y = Random.Range(-(int)radius, (int)radius);

        var kursun = Instantiate(KursunPrefab);
        
        kursun.transform.position = new Vector3(x, y, zPoint);
    }
    
    public void CreateO2(GameObject tube, float zPoint)
    {
        
        var bounds = tube.GetComponent<Collider>().bounds;
        
        var ray = new Ray(new Vector3(bounds.center.x, bounds.center.y, zPoint), Vector3.left);
        Physics.Raycast(ray, out hit);

        var radius = Math.Abs(hit.point.x - bounds.center.x)*0.8f;
        var x = Random.Range(-(int)radius, (int)radius);
        var y = Random.Range(-(int)radius, (int)radius);

        var o2 = Instantiate(O2Prefab);
        
        o2.transform.position = new Vector3(x, y, zPoint);
    }
    
    public void CreateSponge(GameObject tube, float zPoint)
    {
        var bounds = tube.GetComponent<Collider>().bounds;
        
        var ray = new Ray(new Vector3(bounds.center.x, bounds.center.y, zPoint), Vector3.left);
        Physics.Raycast(ray, out hit);

        var radius = Math.Abs(hit.point.x - bounds.center.x)*0.8f;
        var x = Random.Range(-(int)radius, (int)radius);
        var y = Random.Range(-(int)radius, (int)radius);

        var sponge = Instantiate(SpongePrefab);
        
        sponge.transform.position = new Vector3(x, y, zPoint);
    }
    
    public void CreateVirus(GameObject tube, float zPoint)
    {
        var bounds = tube.GetComponent<Collider>().bounds;
        
        var ray = new Ray(new Vector3(bounds.center.x, bounds.center.y, zPoint), Vector3.left);
        Physics.Raycast(ray, out hit);

        var radius = Math.Abs(hit.point.x - bounds.center.x)*0.8f;
        var x = Random.Range(-(int)radius, (int)radius);
        var y = Random.Range(-(int)radius, (int)radius);

        var virus = Instantiate(VirusPrefab);
        
        virus.transform.position = new Vector3(x, y, zPoint);
    }
    
    
    
    /*public void CreateAnt(float zPoint, int number)
    {
        ants = new GameObject[number];
        
        var x = random.Next(-100, 100) / 100f;
        var y = random.Next(-100, 100) / 100f;

        Vector2 distance;

        for (var i = 0; i < number; i++)
        {   
            ants[i] = Instantiate(antPrefab);
            
            distance = new Vector2(x+i/10f, y);
            distance = distance.normalized * radius * 0.8f;

            var ray = new Ray(ants[i].transform.position, distance);
            Physics.Raycast(ray, out hit);

            ants[i].transform.position = new Vector3(hit.point.x, hit.point.y, zPoint);
            var vector = new Vector2(hit.point.x, hit.point.y) -
                         new Vector2(GetComponent<Collider>().bounds.center.x, GetComponent<Collider>().bounds.center.y);
            //Debug.Log(Vector2.SignedAngle(Vector2.right, vector));
            //ants[i].transform.rotation = Quaternion.Euler(0, 0, -Vector2.SignedAngle(Vector2.right, vector));
            
            ants[i].transform.up = new Vector3(GetComponent<Collider>().bounds.center.x, GetComponent<Collider>().bounds.center.y, zPoint);
            ants[i].transform.LookAt(Vector3.back);
        }
    }*/
    
}
