using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MineralBar : MonoBehaviour
{
    public Slider slider;
    
    private void Awake()
    {
        slider.value = 50f;
    }

    public void SetMineral(int mineral)
    {
        slider.value = mineral;
    }
    
    public int GetMineral()
    {
        return (int)(slider.value);
    }


}
