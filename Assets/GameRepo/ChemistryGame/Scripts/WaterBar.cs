using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaterBar : MonoBehaviour
{
    public Slider slider;

    private void Awake()
    {
        slider.value = 30f;
    }

    public void SetWater(int water)
    {
        slider.value = water;
    }
    
    public int GetWater()
    {
        return (int)(slider.value);
    }
    
    
}
