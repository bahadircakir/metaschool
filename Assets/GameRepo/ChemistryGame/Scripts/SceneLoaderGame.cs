using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoaderGame : MonoBehaviour
{
    IEnumerator EndGameCoroutine()
    {
        var operation = SceneManager.LoadSceneAsync("Game End", LoadSceneMode.Single);
        operation.allowSceneActivation = false;
        while (!operation.isDone) {
            if (operation.progress >= 0.9f)
            {
                operation.allowSceneActivation = true;
            }
            yield return null;
        }
    }
    public void EndGameScene()
    {
        StartCoroutine(EndGameCoroutine());
    }
    
}
