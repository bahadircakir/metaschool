using TMPro;
using UnityEngine;

public class DropController : MonoBehaviour
{
    
    private TMP_Text WaterScoreText;
    private GameObject WaterScoreTxtObject;
    private GameObject Player;
    public WaterBar waterbar;

    void Start()
    {
        waterbar = GameObject.Find("WaterBar").GetComponent<WaterBar>();
        Player = GameObject.FindGameObjectWithTag("Player");
        WaterScoreTxtObject = GameObject.Find("WaterScore");
    }
    void Update()
    {
        if (transform.position.z -JoystickPlayerExample.instance.transform.position.z < Random.Range(15,20))
        {
            Invoke("DestroyThis",7f);
            AudioManager.instance.Play("Damla_Dusme");
            GetComponent<MeshRenderer>().enabled = true;
            transform.Translate(Vector3.down * Time.deltaTime*5);
        }

    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            transform.GetChild(0).GetComponent<ParticleSystem>().Play();
            WaterScoreText = WaterScoreTxtObject.GetComponent<TMP_Text>();
            if (waterbar.GetWater() < 100)
            {
                Score.waterScore+=10;
                waterbar.SetWater(waterbar.GetWater()+10);
                AudioManager.instance.Play("Water_Collision");
            }
            
            if(float.Parse(WaterScoreText.text) < 100)
                WaterScoreText.text = (float.Parse(WaterScoreText.text)+10.0f).ToString();
            Debug.Log(Player.transform.localScale);
            Player.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
            JoystickPlayerExample.instance.speed += 2f;
            Camera.main.fieldOfView -= 5f;
            Invoke("DestroyThis",0.001f);
        }
    }

    private void DestroyThis()
    {
        Destroy(this.gameObject);
    }
}
