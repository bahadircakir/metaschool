using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoaderMainMenu : MonoBehaviour
{

    public GameObject mainMenu;
    public GameObject waterDroplet;
    
    public void LoadSceneCoroutine()
    {
        mainMenu.SetActive(false);
        waterDroplet.SetActive(true);
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        var operation = SceneManager.LoadSceneAsync("Game", LoadSceneMode.Single);
        operation.allowSceneActivation = false;
        yield return new WaitForSeconds(3f);
        while (!operation.isDone) {
            if (operation.progress >= 0.9f)
            {
                operation.allowSceneActivation = true;
            }
            yield return null;
        }
    }
    
    public void QuitGame()
    {
        Application.Quit();
    }
}
