﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class AntController : MonoBehaviour
{
    public static AntController instance;
    private Vector3 moveDirection;
    private bool triggered;
    private float radius;
    public GameObject tube_1;
    private Collider collider;
    public GameObject antPrefab;
    public GameObject[] ants;
    private RaycastHit hit;

    private void Awake()
    {
        instance = this;
    }

    public void CreateAnt(GameObject tube, float zPoint)
    {
        collider = tube.GetComponent<Collider>();
        var bounds = collider.bounds;
        radius = Math.Abs(bounds.min.x - bounds.center.x);
        ants = new GameObject[10];
        
        var x = Random.Range(-100, 100) / 100f;
        var y = Random.Range(-100, 100) / 100f;

        Vector2 distance;

        for (var i = 0; i < 10; i++)
        {   
            ants[i] = Instantiate(antPrefab);
            
            distance = new Vector2(x+i/10f, y);
            distance = distance.normalized * radius * 0.8f;

            var ray = new Ray(ants[i].transform.position, distance);
            Physics.Raycast(ray, out hit);

            ants[i].transform.position = new Vector3(hit.point.x, hit.point.y, zPoint);
            ants[i].transform.forward = Vector3.back;
            var vector = new Vector2(hit.point.x, hit.point.y) -
                         new Vector2(collider.bounds.center.x, collider.bounds.center.y);
            
            var angle = 180 - Vector2.Angle(new Vector2(collider.bounds.center.x, collider.bounds.center.y) - new Vector2(ants[i].transform.position.x, ants[i].transform.position.y), new Vector2(collider.bounds.center.x, collider.bounds.center.y) - new Vector2(collider.bounds.max.x, collider.bounds.max.y));
            Debug.Log(angle);
            ants[i].transform.rotation = Quaternion.Euler(ants[i].transform.eulerAngles.x, ants[i].transform.eulerAngles.y, angle);
        }
    }
}