﻿using PathCreation;
using TMPro;
using UnityEngine;

public class JoystickPlayerExample : MonoBehaviour
{
    public float speed;

    private TMP_Text WaterScoreText;
    public Material playerMaterial;
    public VariableJoystick variableJoystick;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    private GameObject WaterScoreTxtObject;
    /*
    private float gravityValue = -9.81f;*/
    private Rigidbody _rigidbody;
    public static JoystickPlayerExample instance;
    public PathCreator pathCreator;
    public EndOfPathInstruction endOfPathInstruction;
    private float distanceTravelled;
    private Vector3 lastPoint;
    private TMP_Text MineralScoreText;
    private GameObject MineralScoreTxtObject;
    public WaterBar waterbar;
    public MineralBar mineralBar;
    public SceneLoaderGame sceneLoaderGame;

    private void Start()
    {
        instance = this;
        lastPoint = transform.position;
        MineralScoreTxtObject = GameObject.Find("MineralScore");
        waterbar = GameObject.Find("WaterBar").GetComponent<WaterBar>();
        WaterScoreTxtObject = GameObject.Find("WaterScore");
    }
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
    public void FixedUpdate()
    {
        distanceTravelled += Vector3.Dot(new Vector3(transform.forward.x, 0, transform.forward.z), transform.position - lastPoint);
        lastPoint = transform.position;
        transform.LookAt(pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction));
        transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);
        var locVel = transform.InverseTransformDirection(_rigidbody.velocity);
        locVel.x = variableJoystick.Horizontal * speed;
        locVel.y = variableJoystick.Vertical * speed;
        locVel.z = speed;
        _rigidbody.velocity = transform.TransformDirection(locVel);
    }

   private void OnCollisionEnter(Collision collision)
    {
            MineralScoreText = MineralScoreTxtObject.GetComponent<TMP_Text>();
            float mineralScore = float.Parse(MineralScoreText.text);
            if (collision.gameObject.CompareTag("HarmfulMineral"))
            {
                Debug.Log(mineralScore);
                if (mineralScore == 0)
                {
                    AudioManager.instance.Play("Bad_Finish");
                    sceneLoaderGame.EndGameScene();
                }

                if (mineralScore > 0)
                {
                    AudioManager.instance.Play("Bad_Mineral");
                    MineralScoreText.text = (mineralScore-10.0f).ToString();
                }
                
                Destroy(collision.gameObject);
            }
            else if (collision.gameObject.CompareTag("BeneficalMineral"))
            {
                if(mineralScore < 100)
                {
                    AudioManager.instance.Play("Good_Mineral");
                    MineralScoreText.text = (mineralScore + 10.0f).ToString();
                }
                Destroy(collision.gameObject);
            }
            else if (collision.gameObject.CompareTag("Monster"))
            {
                WaterScoreText = WaterScoreTxtObject.GetComponent<TMP_Text>();
                if(float.Parse(WaterScoreText.text) > 0)
                    WaterScoreText.text = (float.Parse(WaterScoreText.text)-10.0f).ToString();
                transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
                speed -= 2f;
                Camera.main.fieldOfView += 5f;
                Destroy(collision.gameObject);
            }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Equals("Level1Ending"))
        {
            GameController.instance.checkForLevel1End = true;
        }
        if (other.gameObject.name.Equals("Level2Ending"))
        {
            GameController.instance.checkForLevel2End = true;
        }
        if (other.gameObject.name.Equals("Level3Ending"))
        {
            GameController.instance.checkForLevel3End = true;
        }
    }
}