using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTestGAI : MonoBehaviour
{
    [HideInInspector] public FootballPlayerGAI footballPlayer;

    private Rigidbody rb;

    private bool th = false;
    void Start()
    {
        rb = GetComponent<Rigidbody> ();
        Destroy(gameObject, 20f);
    }

    // Update is called once per frame
    void Update()
    {
        if(!th)
        {
            Vector3 v = rb.velocity;
            v.y = 0f;
            v = Vector3.Lerp(v, Vector3.zero, Time.deltaTime * .4f);
            v.y = rb.velocity.y;
            rb.velocity = v;
        }
    }

    public void Throw(Vector3 direction, float speed)
    {
        th = true;
        GetComponent<Rigidbody>().velocity = direction * speed;

        Invoke("SayMiss", 3f);
    }

    private void SayMiss()
    {
        if (footballPlayer != null) footballPlayer.MissGoal = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.gameObject.name == "Goal")
        {
            if(footballPlayer != null) footballPlayer.ThatsGoal = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Miss")
        {
            if (footballPlayer != null) footballPlayer.MissGoal = true;
        }
    }
}
