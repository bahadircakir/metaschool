using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[SelectionBase]
public class FootballPlayerGAI : MonoBehaviour
{
    [HideInInspector] public Transform freeAreaCenter;
    [HideInInspector] public Vector3 freeTargetPosition = Vector3.zero;
    [HideInInspector] public Transform readyPosition;
    [HideInInspector] public float freeAreaRadius = 1.0f;
    [HideInInspector] public Transform ball;
    [HideInInspector] public Transform goal;
    [HideInInspector] public HakemGAI hakem;
    [HideInInspector] public bool StartGAI = false;
    [HideInInspector] public bool PlayerResponse = false;
    [HideInInspector] public bool ThatsGoal = false;
    [HideInInspector] public bool MissGoal = false;
    [HideInInspector] public float AngleCorrection = 20f;
    [HideInInspector] public float BallPositionCorrection = 1.2f;

    private Animator animator;

    private SoccerPlayerState state = SoccerPlayerState.Free;

    private int freeSwitch = 0;
    private int readySwitch = 0;
    private int shootSwitch = 0;
    private bool applyPause = false;
    private float shootLerp = .1f;
    private float afterShootTimer = 9999f;


    void Start()
    {
        animator = GetComponent<Animator>();
    }
    void Update()
    {
        if (!StartGAI) return;

        ApplyPauseMode();
        ResetValues();

        switch (state)
        {
            case SoccerPlayerState.Free: FreeAI(); readySwitch = 0; shootSwitch = 0; break;
            case SoccerPlayerState.Ready: ReadyAI(); freeSwitch = 0; shootSwitch = 0; break;
            case SoccerPlayerState.Shoot: ShootAI(); readySwitch = 0; freeSwitch = 0; break;
        }
    }

    #region State Controllers
    public void UpdateState(SoccerPlayerState state)
    {
        this.state = state;
    }
    private void FreeAI()
    {
        switch (freeSwitch)
        {
            case 0:
                FreeWait();
                CheckFreePosDistance();
                break;
            case 1:
                GenerateFreePosition(); freeSwitch++;
                break;
            case 2:
                FreeWalk();
                break;
            case 3:
                FreeTurn();
                break;
        }
    }
    private void ReadyAI()
    {
        switch (readySwitch)
        {
            case 0: ReadyWalk(); break;
            case 1: ReadyWait(); break;
        }
    }
    private void ShootAI()
    {
        switch (shootSwitch)
        {
            case 0: CheckPosition(); break;
            case 1: GoShoot(2.2f, BallPositionCorrection); break;
            case 2: AffectTheBall(); break;
            case 3: WaitResponse(); break;
            case 4: ResponseGoal(); break;
            case 5: ResponseMiss(); break;
            case 6: BeFree(); break;
        }
    }
    private void ApplyPauseMode(bool ForceToApply = false)
    {
        if (hakem.State != GAIState.Pause && !ForceToApply)
        {
            applyPause = false;
            return;
        }

        if (applyPause) return;
        applyPause = true;

        animator.Rebind();
        animator.Update(0f);
        animator.SetFloat("Velocity", 200f);
        state = SoccerPlayerState.Free;
    }
    #endregion
    #region Free Motion
    private void CheckFreePosDistance()
    {
        if (Vector3.Distance(transform.position, new Vector3(freeAreaCenter.position.x, transform.position.y, freeAreaCenter.position.z)) > freeAreaRadius) freeSwitch++;
    }
    private void GenerateFreePosition()
    {
        float x = Random.Range(-1f, 1f);
        float z = Random.Range(-1f, 1f);

        freeTargetPosition = new Vector3(x, transform.position.y, z).normalized * freeAreaRadius * Random.Range(.2f, .7f) + new Vector3(freeAreaCenter.position.x, transform.position.y, freeAreaCenter.position.z);
    }
    private void FreeWalk()
    {
        if (!animator.GetBool("Idle To Walk")) animator.SetBool("Idle To Walk", true);

        float distance = Vector3.Distance(transform.position, new Vector3(freeTargetPosition.x, transform.position.y, freeTargetPosition.z));
        if (distance < .5f)
        {
            freeSwitch++;
            animator.SetFloat("Direction Angle Offset", 0f);
        }
        else
        {
            if (distance > 4f && hakem.State == GAIState.Start)
            {
                animator.SetFloat("Velocity", animator.GetFloat("Velocity"), 200f, Time.deltaTime * 5f);
            }
            else
            {
                animator.SetFloat("Velocity", Mathf.Lerp(animator.GetFloat("Velocity"), 100f, Time.deltaTime * 5f));
            }

            float turnValue = AngleBetweenOwnDirectionToTargetDirection(freeTargetPosition);

            afterShootTimer += Time.deltaTime;
            if(afterShootTimer < 2f)
            {
                if (hakem.TurnBackMethod == TurnBack.Random) animator.SetFloat("Direction Angle Offset", turnValue);
                else if (hakem.TurnBackMethod == TurnBack.LeftSide)
                {
                    if (turnValue < 0) animator.SetFloat("Direction Angle Offset", turnValue);
                    else animator.SetFloat("Direction Angle Offset", -(360f + turnValue));
                }
                else if (hakem.TurnBackMethod == TurnBack.RightSide)
                {
                    if (turnValue > 0) animator.SetFloat("Direction Angle Offset", turnValue);
                    else animator.SetFloat("Direction Angle Offset", (360f - turnValue));
                }
            }
            else
            {
                animator.SetFloat("Direction Angle Offset", turnValue);
            }
        }
    }
    private void FreeTurn()
    {
        animator.SetFloat("Velocity", Mathf.Lerp(animator.GetFloat("Velocity"), 0f, Time.deltaTime * 7f));
        animator.SetBool("Turn Around", true);

        Vector3 LookDir = (goal.transform.position - transform.position).normalized;
        LookDir.y = 0f;

        float angle = Vector3.Angle(transform.forward, LookDir);

        if (angle > 1f)
        {
            float dir = 1f;
            Vector3 cross = Vector3.Cross(transform.forward, LookDir);
            if (cross.y < 0f) dir = -1f;

            animator.SetFloat("Turn Value", dir);
        }
        else
        {
            freeSwitch = 0;
        }
    }
    private void FreeWait()
    {
        animator.SetFloat("Turn Value", Mathf.Lerp(animator.GetFloat("Turn Value"), 0f, Time.deltaTime * 5f));
        if (animator.GetBool("Idle To Walk")) animator.SetBool("Idle To Walk", false);
        if (animator.GetBool("Turn Around")) animator.SetBool("Turn Around", false);
    }
    #endregion
    #region Ready Motion
    private void SetDirection()
    {
        if (ball == null)
        {
            LookAtTheTarget(readyPosition, 2f);
        }
        else
        {
            Vector3 target = ball.transform.position + ball.GetComponent<Rigidbody>().velocity;
            target.y = transform.position.y;
            target.z = readyPosition.position.z;
            LookAtTheTarget(target, 2f);
        }
    }
    private void ReadyWalk()
    {
        if (animator.GetBool("Turn Around"))
        {
            if (ball != null) LookAtTheTarget(new Vector3(ball.position.x, ball.position.y, readyPosition.position.z), 7f);
            else LookAtTheTarget(readyPosition, 7f);
            animator.SetBool("Turn Around", false);
        }

        if (!animator.GetBool("Idle To Walk"))
        {
            animator.SetBool("Idle To Walk", true);
        }

        if (Mathf.Abs(transform.position.z - readyPosition.position.z) < 1f)
        {
            readySwitch++;
            hakem.Settings.ready = true;
            animator.SetFloat("Direction Angle Offset", 0f);
        }
        else
        {
            animator.SetFloat("Velocity", 200f);
            SetDirection();
        }

    }
    private void ReadyWait()
    {
        if(!hakem.EnableAngleCorrection)
        {
            state = SoccerPlayerState.Shoot;
            animator.SetBool("Fast Shoot", true);
            return;
        }

        if (animator.GetFloat("Turn Value") != 0f) animator.SetFloat("Turn Value", 0f);
        if (!animator.GetBool("Idle Ready")) animator.SetBool("Idle Ready", true);
        if (animator.GetBool("Turn Around")) animator.SetBool("Turn Around", false);
    }  
    #endregion
    #region Shoot Motion
    private void CheckPosition()
    {
        if(!hakem.EnableAngleCorrection)
        {
            animator.SetBool("Linear Done", true);
            shootSwitch++;
            return;
        }

        Vector3 ballPos = ball.position - ball.GetComponent<Rigidbody>().velocity.normalized / 2f;
        Vector3 line1 = goal.position - ballPos;
        Vector3 line2 = ballPos - transform.position;
        float angle = Vector3.SignedAngle(line1, line2, Vector3.up);

        Vector3 lookAt = (ball.position + ball.GetComponent<Rigidbody>().velocity.normalized * 2f - transform.position).normalized;
        lookAt.y = 0f;
        transform.forward = Vector3.Lerp(transform.forward, lookAt, Time.deltaTime * 2f);

        float ballVelocity = ball.GetComponent<Rigidbody>().velocity.magnitude;

        if (ballVelocity > 3f) return;
        if (Mathf.Abs(angle) < AngleCorrection && ballVelocity < 2f)
        {
            animator.SetBool("Linear Done", true);
            shootSwitch++;
        }
        else
        {
            if (angle < 0)
            {
                animator.SetInteger("Set Linear Position", -1);

                transform.forward = lookAt;
            }
            else
            {
                animator.SetInteger("Set Linear Position", 1);

            }

        }
    }
    private void GoShoot(float untilDistance, float ballPositionOffset)
    {
        if(ball == null) return;

        Vector3 line1 = goal.position - transform.position;
        Vector3 line2 = ball.position - transform.position;
        float angle = Vector3.SignedAngle(line1, line2, Vector3.up);

        Vector3 target = ball.transform.position;
        target.x += ballPositionOffset * Vector3.Distance(ball.position, transform.position) * .05f;
        target.y = transform.position.y;
        target.z -= (ball.position.z - transform.position.z) / 2f;

        LookAtTheTarget(target, Mathf.Lerp(shootLerp, 15f, Time.deltaTime * 3f));

        float distance_Goal_Own = Vector3.Distance(goal.position, transform.position);
        float distance_Goal_Ball = Vector3.Distance(goal.position, ball.position);
        
        if (Vector3.Distance(transform.position, ball.position) < untilDistance || distance_Goal_Own < distance_Goal_Ball)
        {
            if (angle < 0f) animator.SetBool("Shoot Left", true);
            else animator.SetBool("Shoot Right", true);
            shootLerp = .1f;
            shootSwitch++;
        }
        else
        {
            animator.SetFloat("Velocity", 200f);
        }
    }
    private void AffectTheBall()
    {
        LookAtTheTarget(ball, 2f);

        if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Shoot Right") && !animator.GetCurrentAnimatorStateInfo(0).IsName("Shoot Left")) return;


        float frame = animator.GetCurrentAnimatorClipInfo(0)[0].clip.length *
                                    (animator.GetCurrentAnimatorStateInfo(0).normalizedTime % 1) *
                                     animator.GetCurrentAnimatorClipInfo(0)[0].clip.frameRate;

        if (frame > 12f)
        {
            ball.GetComponent<BallController>().ThrowBall();
            ball.GetComponent<BallController>().footballPlayer = this;
            hakem.Settings.shoot = true;
            shootSwitch++;
            MissGoal = false;
            ThatsGoal = false;
            SoundController.Instance.PlaySoccerBallKick();
        }

    }
    private void WaitResponse()
    {
        if (!PlayerResponse)
        {
            animator.SetBool("Not Response", true);
            shootSwitch = 6;
        }
        else if (ThatsGoal)
        {
            shootSwitch = 4;
        }
        else if (MissGoal)
        {
            shootSwitch = 5;
        }
    }
    private void ResponseGoal()
    {
        animator.SetInteger("Victory", Random.Range(0, 5));
        shootSwitch = 6;
    }
    private void ResponseMiss()
    {
        animator.SetInteger("Miss", Random.Range(0, 2));
        shootSwitch = 6;
    }
    private void BeFree()
    {
        afterShootTimer = 0f;
        state = SoccerPlayerState.Free;
    }

    #endregion
    #region Calculators
    private float AngleBetweenOwnDirectionToTargetDirection(Vector3 targetPosition)
    {
        float dir = 1f;
        targetPosition.y = 0f;
        Vector3 my2DPos = transform.position;
        my2DPos.y = 0f;
        Vector3 cross = Vector3.Cross(transform.forward, (targetPosition - my2DPos).normalized);
        if (cross.y < 0f) dir = -1f;

        return dir * Vector3.Angle(transform.forward, (targetPosition - my2DPos).normalized);
    }
    private void LookAtTheTarget(Transform target, float timeScale)
    {
        Vector3 lookAt = target.position - transform.position;
        lookAt.y = 0f;
        transform.forward = Vector3.Lerp(transform.forward, lookAt, Time.deltaTime * timeScale);
    }
    private void LookAtTheTarget(Vector3 target, float timeScale)
    {
        Vector3 lookAt = target - transform.position;
        lookAt.y = 0f;
        transform.forward = Vector3.Lerp(transform.forward, lookAt, Time.deltaTime * timeScale);
    }
    private void ResetValues()
    {

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Motion 0"))
        {
            if (animator.GetBool("Linear Done")) animator.SetBool("Linear Done", false);
            if (animator.GetBool("Idle Ready")) animator.SetBool("Idle Ready", false);
            if (animator.GetBool("Fast Shoot")) animator.SetBool("Fast Shoot", false);
            if (animator.GetInteger("Set Linear Position") != 0) animator.SetInteger("Set Linear Position", 0);
        }
        else if (animator.GetCurrentAnimatorStateInfo(0).IsName("Motion"))
        {
            if (animator.GetInteger("Victory") != -1) animator.SetInteger("Victory", -1);
            if (animator.GetInteger("Miss") != -1) animator.SetInteger("Miss", -1);
        }
        else if(animator.GetCurrentAnimatorStateInfo(0).IsName("Shoot Left") || animator.GetCurrentAnimatorStateInfo(0).IsName("Shoot Right"))
        {
            if (animator.GetBool("Shoot Left")) animator.SetBool("Shoot Left", false);
            if (animator.GetBool("Shoot Right")) animator.SetBool("Shoot Right", false);
        }

        if (PlayerResponse)
        {
            if (!animator.GetBool("Not Response")) animator.SetBool("Not Response", true);
        }
        else
        {
            if (animator.GetBool("Not Response")) animator.SetBool("Not Response", false);
        }
    }
    #endregion
}

public enum SoccerPlayerState
{
    Free,
    Ready,
    Shoot
}


