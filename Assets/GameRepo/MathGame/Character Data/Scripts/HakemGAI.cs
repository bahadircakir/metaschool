using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[SelectionBase]
public class HakemGAI : MonoBehaviour
{
    #region Variables
    #region Attributes
    [Tooltip("Start: Oyun Baslar\nStop: Oyuncular yok olur(End Game)\nPause: Oyuncular Tekrar start komutunu bekler(Pause Menu)\n\nGetState();\nSetState()\nile degerle islem yapilir."), Space(10)]
    #endregion
    private GAIState _State = GAIState.Stop;

    public GAIState State
    {
        get { return _State; }
        set
        {
            _State = value;
            if (_State == GAIState.Stop)
            {
                Settings.Destruct();
            }
        }
    }
    #region Attributes
    [Tooltip("Top atma makinasinin ana renklerine gore stiller")]
    #endregion
    public RefereeMachineStyle RefereeStyle = RefereeMachineStyle.Yellow;
    #region Attributes
    [Tooltip("Oyuncular geri donerken hangi yonu tercih edecek?")]
    #endregion
    public TurnBack TurnBackMethod;
    #region Attributes
    [Tooltip("Hakemin topu yere birakma hizi"), Min(1f)]
    #endregion
    public float ballThrowSpeed = 5f;
    #region Attributes
    [Tooltip("Bu deger True oldugunda, oyuncular sut cektikten sonra gol olup olmamasina gore tepki verir, daha sonra yerine doner.")]
    #endregion
    public bool EnablePlayerResponse = true;
    #region Attributes
    [Tooltip("Oyuncular sut cekmeden once (kendisi-top-kale) dogrultusunu asagidaki aci sinirinda ayarlar")]
    #endregion
    public bool EnableAngleCorrection = true;
    #region Attributes
    [Tooltip("Hakem Topu atmadan once ekstra bekleme suresi.  \"Enable Angle Correction\" aktif degilse bu deger 0 alinir."), Range(0f, 10f)]
    #endregion
    public float WaitBeforeThrowTheBall = 0f;

    [Space(20), Header("Asagidakileri \"Stop\" modda degistirin:")]
    #region Attributes
    [Tooltip("Oyuncularin hangi formayla olusacagini sec. Tek hakem varsa random daha dogru olur.")]
    #endregion
    public PlayerSkin PlayersSkin = PlayerSkin.Random;
    #region Attributes
    [Space(5), Tooltip("Bu hakeme bagli toplam oyuncu sayisi"), Min(2)]
    #endregion
    public int PlayerCount = 5;
    #region Attributes
    [Tooltip("Oyuncularin spawn alaninin yaricapi"), Min(2f)]
    #endregion
    public float spawnRadius = 2f;

    [Space(20), SerializeField] public _Settings_ Settings;
    #endregion
    #region Unity Methods
    private void Awake()
    {
        Settings.SetHakem(this, RefereeStyle);
    }
    private void OnEnable()
    {
        Settings.Initialize();
    }

    private void Update()
    {
        Settings.Update();
    }
    #endregion
    #region Helpers
    public void UEDestroy(GameObject g, float time = 0f)
    {
        Destroy(g, time);
    }
    public GameObject UEInstantiate(GameObject g, Vector3 position, Quaternion rotation, Transform parent = null)
    {
        return Instantiate(g, position, rotation, parent);
    }
    #endregion
}

[System.Serializable]
public class _Settings_
{
    private HakemGAI hakem;

    public Transform BallInstantiatePosition;

    [Space(5), Tooltip("Oyuncunun top hareketine gore olan pozisyonunun offset degeri"), Range(-3f, 3f)]
    public float BallPositionCorrection = 1.2f;



    [Tooltip("Oyuncular sut cekmeden once (kendisi-top-kale) dogrultusunu bu aci sinirinda ayarlar"), Range(1f, 50f)]
    public float AngleCorrectionValue = 20f;



    [Space(10), Header("Data")]
    public Transform Goal;

    public GameObject BallPrefab;
    public GameObject CharacterPrefab;

    public List<SkinMaterials> CharacterMaterials;

    [Tooltip("Oyuncularin spawn edilecegi alanin merkezi")]
    public Transform CharacterSpawnCenter;

    [Tooltip("Oyuncularin sut cekmeden once hazirlik yaptigi nokta. Bu deger X ekseni boyunca uzanan bir sinir gibi davranir.")]
    public Transform CharacterReadyPos;

    public List<Material> RefereeMaterials;

    private Vector3[] spawnPositions;
    private GameObject[] players;

    private GameObject PlayersFolder;

    private int switcher = 0;
    private float timer = 0f;

    private int playerIndex = 0;
    private int ballIndex = 0;
    [HideInInspector] public bool ready = false;
    [HideInInspector] public bool shoot = false;

    private GAIState oldState = GAIState.Stop;
    private GameObject oldBall;

    private RefereeMachineStyle oldStyle;

    public void SetHakem(HakemGAI hakem, RefereeMachineStyle style)
    {
        this.hakem = hakem;
        oldStyle = style;
        SetStyle(style);

    }
    public void Update()
    {
        StateTestKeys();
        CheckStyle();

        if (hakem.EnablePlayerResponse && !hakem.EnableAngleCorrection) hakem.EnableAngleCorrection = true;
        if (oldState != hakem.State) StateController();
        if (hakem.State != GAIState.Start) return;

        switch (switcher)
        {
            case 0: SetPlayerStateToReady(); break;
            case 1: Wait(hakem.EnableAngleCorrection ? hakem.WaitBeforeThrowTheBall : 0f); break;
            case 2: ThrowBall(hakem.ballThrowSpeed); break;
            case 3: WaitReady(); break;
            case 4: SetPlayerStateToShoot(); break;
            case 5: WaitShoot(); break;
            case 6: ChangeFocusedPlayerIndex(); break;
            case 7: switcher = 0; break;
        }
    }

    #region States
    private void StateController()
    {
        if (hakem.State == GAIState.Start)
        {
            if (oldState == GAIState.Stop)
            {
                if (players == null) Initialize();
                StartGame();
            }
        }
        else if (hakem.State == GAIState.Stop)
        {
            //Destruct();
        }
        else if (hakem.State == GAIState.Pause)
        {
            if (oldState != GAIState.Stop) Pause();
            else hakem.State = GAIState.Stop;
        }

        oldState = hakem.State;
    }
    private void StateTestKeys()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1)) hakem.State = GAIState.Start;
        else if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2)) hakem.State = GAIState.Stop;
        else if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3)) hakem.State = GAIState.Pause;
        else if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.D) && oldBall != null)
        {
            hakem.UEDestroy(oldBall);
        }
    }
    private void CheckStyle()
    {
        if (hakem.RefereeStyle != oldStyle)
        {
            oldStyle = hakem.RefereeStyle;
            SetStyle(hakem.RefereeStyle);
        }
    }
    private void SetStyle(RefereeMachineStyle style)
    {
        MeshRenderer mr = hakem.transform.Find("FBX_BallThrower").GetComponent<MeshRenderer>();
        Material[] ms = new Material[3];

        if(style == RefereeMachineStyle.Random)
        {
            style = (RefereeMachineStyle)Random.Range(1, 5);
        }

        if (style == RefereeMachineStyle.Yellow)
        {

            ms[0] = RefereeMaterials[3];
            ms[2] = RefereeMaterials[2];
            ms[1] = RefereeMaterials[0];
        }
        else if (style == RefereeMachineStyle.Red)
        {
            ms[0] = RefereeMaterials[3];
            ms[2] = RefereeMaterials[3];
            ms[1] = RefereeMaterials[1];
        }
        else if (style == RefereeMachineStyle.Blue)
        {
            ms[0] = RefereeMaterials[1];
            ms[2] = RefereeMaterials[3];
            ms[1] = RefereeMaterials[2];
        }
        else if (style == RefereeMachineStyle.Black)
        {
            ms[0] = RefereeMaterials[1];
            ms[2] = RefereeMaterials[1];
            ms[1] = RefereeMaterials[3];
        }
        mr.sharedMaterials = ms;
    }
    #endregion
    #region GAI
    private void Wait(float waitTime)
    {
        timer += Time.deltaTime;
        if (timer >= waitTime)
        {
            timer = 0f;
            switcher++;
        }
    }
    private void SetPlayerStateToReady()
    {
        players[playerIndex].GetComponent<FootballPlayerGAI>().UpdateState(SoccerPlayerState.Ready);
        switcher++;
    }
    private void WaitReady()
    {
        if (ready)
        {
            ready = false;
            switcher++;
        }
    }
    private void SetPlayerStateToShoot()
    {
        players[playerIndex].GetComponent<FootballPlayerGAI>().PlayerResponse = hakem.EnablePlayerResponse;
        players[playerIndex].GetComponent<FootballPlayerGAI>().UpdateState(SoccerPlayerState.Shoot);
        switcher++;
    }
    private void WaitShoot()
    {
        if (shoot)
        {
            shoot = false;
            switcher++;
        }
    }
    private void ChangeFocusedPlayerIndex()
    {
        if (players == null) return;

        playerIndex = (playerIndex + 1) % players.Length;
        ready = false;
        shoot = false;
        switcher++;
    }
    private void ThrowBall(float speed)
    {
        GameObject ball = hakem.UEInstantiate(BallPrefab, BallInstantiatePosition.position, Quaternion.identity);
        ball.name = "Ball " + ballIndex.ToString();
        ballIndex++;

        players[playerIndex].GetComponent<FootballPlayerGAI>().ball = ball.transform;

        Rigidbody rb = ball.GetComponent<Rigidbody>();
        rb.velocity = hakem.transform.forward * Random.Range(speed - .5f, speed + .5f);

        oldBall = ball;
        switcher++;
    }
    #endregion
    #region Initialize Methods
    public void Initialize()
    {
        CreatePlayersFolderObject();
        spawnPositions = GenerateSpawnPositions();
        players = SpawnPlayers();
        SetPlayersSkin();
        SetPlayersScript();
    }
    private void CreatePlayersFolderObject()
    {
        PlayersFolder = new GameObject("Players");
        PlayersFolder.transform.position = Vector3.zero;
        PlayersFolder.transform.rotation = Quaternion.identity;
        PlayersFolder.transform.localScale = Vector3.one;
    }
    private Vector3[] GenerateSpawnPositions()
    {
        float angle = 0f;
        Vector3[] Positions = new Vector3[hakem.PlayerCount];

        for (int i = 0; i < hakem.PlayerCount; i++)
        {
            float distance = Random.Range(hakem.spawnRadius * .2f, hakem.spawnRadius * .9f);
            Debug.Log("Goal.position");
            Debug.Log(Goal.position);
            Vector3 pos = (Goal.position - CharacterSpawnCenter.position).normalized;
            pos = Quaternion.AngleAxis(angle, Vector3.up) * pos;
            pos = CharacterSpawnCenter.position + pos * distance;
            Positions[i] = pos;
            angle += 360f / hakem.PlayerCount;
        }
        return Positions;
    }
    private GameObject[] SpawnPlayers()
    {
        GameObject[] ps = new GameObject[hakem.PlayerCount];

        for (int i = 0; i < hakem.PlayerCount; i++)
        {
            GameObject p = hakem.UEInstantiate(CharacterPrefab, spawnPositions[i], Quaternion.identity, PlayersFolder.transform);
            p.name = "Player " + (i + 1).ToString();
            p.transform.localScale = Random.Range(.95f, 1.05f) * Vector3.one;
            ps[i] = p;
        }
        return ps;
    }
    private void SetPlayersSkin()
    {
        int skinStyle = 0;

        if (hakem.PlayersSkin == PlayerSkin.Random) skinStyle = Random.Range(0, CharacterMaterials.Count);
        else skinStyle = (int)hakem.PlayersSkin - 1;

        int skinoffset = Random.Range(0, CharacterMaterials[skinStyle].materials.Count);
        for (int i = 0; i < hakem.PlayerCount; i++)
        {
            players[i].transform.GetComponentInChildren<SkinnedMeshRenderer>().sharedMaterial
                = CharacterMaterials[skinStyle].materials[(i + skinoffset) % CharacterMaterials[skinStyle].materials.Count];
        }
    }
    private void SetPlayersScript()
    {
        for (int i = 0; i < hakem.PlayerCount; i++)
        {
            FootballPlayerGAI f = players[i].GetComponent<FootballPlayerGAI>();
            f.freeTargetPosition = spawnPositions[i];
            f.freeAreaCenter = CharacterSpawnCenter;
            f.freeAreaRadius = hakem.spawnRadius;
            f.readyPosition = CharacterReadyPos;
            f.goal = Goal;
            f.hakem = hakem;
            f.AngleCorrection = AngleCorrectionValue;
            f.BallPositionCorrection = BallPositionCorrection;
            Vector3 lookPos = Goal.position;
            lookPos.y = players[i].transform.position.y;
            players[i].transform.LookAt(lookPos);
        }
    }
    private void StartGame()
    {
        for (int i = 0; i < hakem.PlayerCount; i++)
        {
            FootballPlayerGAI f = players[i].GetComponent<FootballPlayerGAI>();
            f.StartGAI = true;
        }
    }

    [System.Serializable]
    public class SkinMaterials
    {
        public List<Material> materials = new List<Material>();
    }
    #endregion
    #region Destruct Methods
    public void Destruct()
    {
        switcher = 0;
        spawnPositions = null;
        DestroyPlayers();
    }
    private void DestroyPlayers()
    {
        if (players != null && players.Length > 0f)
        {
            for (int i = 0; i < players.Length; i++)
            {
                hakem.UEDestroy(players[i]);
            }
            players = null;
        }

        if (players != null)
        {
            players = null;
        }

        if (PlayersFolder != null)
        {
            hakem.UEDestroy(PlayersFolder);
        }

    }
    #endregion
    #region Pause Methods
    private void Pause()
    {
        ChangeFocusedPlayerIndex();
        switcher = 0;
    }
    #endregion
}
public enum GAIState
{
    Start,
    Stop,
    Pause
}
public enum TurnBack
{
    Random,
    RightSide,
    LeftSide
}
public enum PlayerSkin
{
    Random,
    Yellow_Blue,
    Red_Black,
    White_Black
}
public enum RefereeMachineStyle
{
    Random,
    Yellow,
    Red,
    Blue,
    Black
}