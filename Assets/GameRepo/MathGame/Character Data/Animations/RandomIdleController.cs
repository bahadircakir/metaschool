using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomIdleController : MonoBehaviour
{
    private float[] randomTimes = new float[9];
    private float[] timers = new float[9];
    private Animator anim;
    void Awake()
    {
        anim = GetComponent<Animator>();
        for(int i = 0; i < 9; i++)
        {
            randomTimes[i] = Random.Range(0.5f, 1f);
        }
    }

    void Update()
    {
        for(int i = 0;i < 9;i++)
        {
            timers[i] += Time.deltaTime;

            if (timers[i] >= randomTimes[i])
            {
                timers[i] = 0.0f;
                randomTimes[i] = Random.Range(0.5f, 1f);
                anim.SetBool("Idle Random " + (i + 1).ToString(), System.Convert.ToBoolean(Random.Range(0, 2)));
            }
        }
    }



}
