using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class CountDownTimer : MonoBehaviour
{
    public SoundController soundController;
    public GameObject SkillActionTutorialUI;
    
    private RectTransform rt;
    private TextMeshProUGUI tm;
    private bool Lock = false;
    private int switcher = -1;
    private int number = 4;
    private float timer = 0f;

    public Color Three_Color;
    public Color Two_Color;
    public Color One_Color;
    public Color GO_Color;
    private void OnEnable()
    {
        Lock = false;
        switcher = -1;
        number = 4;
        timer = 0f;
    }

    public void StartTimer()
    {
        if (Lock) return;
        Lock = true;
        rt = GetComponent<RectTransform>();
        tm = GetComponent<TextMeshProUGUI>();
        
        SkillManager.instance.UpdateSkills();
    }
    private void DoAfterCompleteCountDown()
    {
        if (!PlayerPrefs.HasKey("SkillActionTutorial"))
        {
            if (SkillDatabase.GetSelectedSkills().Count == 0)
            {
                MathGameController.instance.StartGame();
            }
            else
            {
                SkillActionTutorialUI.SetActive(true);
            }
        }
        else
        {
            MathGameController.instance.StartGame();
        }
    }



    #region Compute
    private void RunCountDown()
    {
        if (!Lock) return;

        switch (switcher)
        {
            case -1:
                if (number == 0)
                {
                    Lock = false;
                    DoAfterCompleteCountDown();
                }
                else
                {
                    number--;
                    switcher++;

                    if (number == 3) tm.color = Three_Color;
                    else if (number == 2) tm.color = Two_Color;
                    else if (number == 1) tm.color = One_Color;
                    else if (number == 0) tm.color = GO_Color;


                }
                break;
            case 0:
                if(number == 0) tm.SetText("GO!");
                else tm.SetText(number.ToString());
                rt.DOScale(1f, .2f);
                switcher++;
                break;
            case 1:
                Timer(.2f);
                break;
            case 2:
                if (number > 0)
                    soundController.PlayCountDownSound(number - 1);
                rt.DOScale(.7f, .4f);
                switcher++;
                break;
            case 3:
                Timer(.4f);
                break;
            case 4:
                rt.DOScale(0f, .4f);
                switcher++;
                break;
            case 5:
                Timer(.4f);
                break;
            case 6:
                switcher = -1;
                break;
        }
    }
    private void Update()
    {
        RunCountDown();
    }
    private void Timer(float WaitTime)
    {
        timer += Time.deltaTime;
        if (timer >= WaitTime)
        {
            timer = 0f;
            switcher++;
        }
    }
    public void Kill()
    {
        gameObject.SetActive(false);
        MathGameController.instance.PauseGame();
        soundController.KillCountDownSounds();
        switcher = -1;
        number = 4;
        Lock = false;
    }

    #endregion
}
