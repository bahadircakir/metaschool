using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class GlovesMenuScript : MonoBehaviour
{
    [Space(10f)]
    public List<GameObject> GloveFrames;
    public Color SelectedColor;
    public Color UnselectedColor;

    public void SelectGlove(int GloveIndex)
    {
        PlayerPrefs.SetInt("GloveIndex", GloveIndex);
        for (int i = 0; i < GloveFrames.Count; i++)
        {
            if (i == GloveIndex) GloveFrames[i].GetComponent<Image>().color = SelectedColor;
            else GloveFrames[i].GetComponent<Image>().color = UnselectedColor;
        }
    }
}
