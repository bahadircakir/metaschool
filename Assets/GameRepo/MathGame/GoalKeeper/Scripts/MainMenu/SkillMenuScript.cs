using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SkillMenuScript : MonoBehaviour
{    
    public Color SelectedColor;
    public Color UnselectedColor;
    public List<Button> SkillButtons;
    public List<GameObject> SkillFrames;
    public List<GameObject> SkillAdImages;
    public List<TextMeshProUGUI> NumberOfSkills;

    public void Start()
    {
        DeselectSkillAll();
    }
    public void SkillMenuSet(){
        try
        {
            var skills = SkillDatabase.GetSelectedSkills();
            for (var i = 0; i < skills.Count; i++)
            {
                int size = SkillDatabase.GetSkillSize(skills[i]);
                if (size > 0) SelectSkill((int)skills[i]);
            }
            
            for (var i = 0; i < NumberOfSkills.Count; i++)
            {
                int size = SkillDatabase.GetSkillSize((Skills)i);
                NumberOfSkills[i].SetText(size.ToString());
                SkillAdImages[i].SetActive(size == 0);
            }
            SkillButtonInteractableController();
        }
        catch { 
            // TODO: TOAST ERROR
        }
    }
    public void SkillClickEvent(int SkillIndex)
    {
        if (SkillDatabase.IsSelected((Skills)SkillIndex))
            DeselectSkill(SkillIndex,1);
        else
            SelectSkill(SkillIndex);
        SkillButtonInteractableController();
    }
    public void SelectSkill(int SkillIndex)
    {
        SkillDatabase.SelectSkill((Skills)SkillIndex);
        SkillFrames[SkillIndex].GetComponent<Image>().color = SelectedColor;
    }
    // mode = 0 -> At game start deselect all is running, mode = 1 -> user clicking a selected skill
    public void DeselectSkill(int SkillIndex, int mode)
    {
        SkillFrames[SkillIndex].GetComponent<Image>().color = UnselectedColor;
    }
    public void DeselectSkillAll()
    {
        for (int i = 0; i < SkillFrames.Count; i++)
            DeselectSkill(i, 0); // mode = 0 -> At game start deselect all is running, mode = 1 -> user clicking a selected skill
    }
    private void SkillButtonInteractableController()
    {
        List<Skills> skills = SkillDatabase.GetSelectedSkills();
        Debug.Log(skills.Count);
        for (int i = 0; i < SkillButtons.Count; i++)
        {
            if (skills.Count < 2 || SkillDatabase.GetSkillSize((Skills)i) == 0)
                SkillButtons[i].interactable = true;
            else 
                SkillButtons[i].interactable = skills.Contains((Skills)i);
        }
    }
}