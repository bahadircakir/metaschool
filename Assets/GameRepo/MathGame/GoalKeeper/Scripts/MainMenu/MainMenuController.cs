using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;
using System.Linq;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public GameObject Game;
    public GameObject GameUI;
    public GameObject EndUI;
    public GameObject MainMenuUI;
    public Animator MainPanelTop;
    public Animator MainPanelLeft;
    public Animator MainPanelUp;
    public Animator MainPanelDown;
    public Image Darker;
    public Image Background;
    public TextMeshProUGUI Header;
    [Space(10f)]    
    public GameObject[] MainPanelFirstScreenButtons;
    public GameObject[] MainPanelPlayButtons;
    public GameObject[] MainPanelPauseButtons;
    [Space(10f)]
    public GameObject OptionsMenu;
    public GameObject GlovesMenu;
    public GameObject SkillMenu;
    public GameObject AISyncPanel;
    public GameObject leaderboardPanel;
    public GameObject LeavePanel;
    [Space(10f)]
    public SoundController soundController;
    [Range(0f, 1f)] public float VideoSequencerTransparencyValue = .4f;
    public CountDownTimer countDownTimer;
    private GlovesMenuScript glovesMenuScript;
    private SkillMenuScript skillMenuScript;
    [Space(10f)]
    public static MainMenuController instance;
    private bool Initialize = false;
    private float InitializeTimer = 0f;
    private float ButtonTimer = 0f;
    [HideInInspector] public int SelectedGloveIndex = 0;
    [HideInInspector] public int SelectedSkillIndex = 0;

    public GameObject userImage;
    public TMP_Text usernamePreviewOptions;
    public TMP_Text usernamePreviewInKeyboardOptions;
    private void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        UpdateUserProfile();
        instance = this;
    }

    private void Start()
    {
        CloseAllSubMenus();
        Close_LoadingCanvas();
        Invoke(nameof(UnloadStarterScene), 1f);
    }
    private void OnEnable()
    {
        glovesMenuScript = GlovesMenu.GetComponent<GlovesMenuScript>();
        skillMenuScript = SkillMenu.GetComponent<SkillMenuScript>();
        OnEnableEvent();
    }
    private void OnDisable()
    {
        countDownTimer.gameObject.SetActive(true);
        countDownTimer.StartTimer();
    }
    private void Update()
    {
        InitializeMenu();
    }
    
    private void UnloadStarterScene()
    {
        if(SceneManager.GetActiveScene().name == "Starter Scene")
        {
            StartCoroutine(UnloadStarterSceneAsync());
        }
        
    }
    IEnumerator UnloadStarterSceneAsync()
    {
        var operation = SceneManager.UnloadSceneAsync("Starter Scene");
        yield return operation;
    }
    
    private void OnEnableEvent()
    {
        Initialize = false;
        soundController.ReduceSFX();
        Darker.DOFade(.5f, .5f);
        Button_Back();
        Background.DOFade(1f, .5f);
    }
    private void InitializeMenu()
    {
        if (!Initialize)
        {
            InitializeTimer += Time.deltaTime;
            if (InitializeTimer > 0.2f)
            {
                MainPanelTop.SetBool("Open", true);
                MainPanelLeft.SetBool("Open", true);

                Initialize = true;
                InitializeTimer = 0f;
            }
        }
    }

    private void ResetMenu()
    {
        MainPanelTop.SetBool("Open", false);
        MainPanelLeft.SetBool("Open", false);
        MainPanelUp.SetBool("Open", false);
        MainPanelDown.SetBool("Open", false);
    }
    private void ButtonShow(GameObject[] buttonArray, int isShow)
    {
        for (var i = 0; i < buttonArray.Length; i++)
            buttonArray[i].SetActive(isShow == 1);
        ButtonTimer = 0f;
    }

    private void ClearMenuElements()
    {
        if (MainPanelUp.GetBool("Open")) MainPanelUp.SetBool("Open", false);
        if (MainPanelDown.GetBool("Open")) MainPanelDown.SetBool("Open", false);
    }
    private void SetMenuElements(string elementName)
    {
        Header.SetText(elementName);
        if (!MainPanelLeft.GetBool("Open")) MainPanelLeft.SetBool("Open", true);
        if (!MainPanelUp.GetBool("Open")) MainPanelUp.SetBool("Open", true);
        if (!MainPanelDown.GetBool("Open")) MainPanelDown.SetBool("Open", true);
        CloseAllSubMenus();
    }
    public void Button_Play()
    {
        ButtonShow(MainPanelFirstScreenButtons, 0);
        ButtonShow(MainPanelPlayButtons, 1);
        ButtonShow(MainPanelPauseButtons, 0);
        Button_Skill();
    }
    public void Button_Back()
    {
        ButtonShow(MainPanelFirstScreenButtons, 1);
        ButtonShow(MainPanelPlayButtons, 0);
        ButtonShow(MainPanelPauseButtons, 0);
        ClearMenuElements();
    }
    public void Button_Pause()
    {        
        ButtonShow(MainPanelFirstScreenButtons, 0);
        ButtonShow(MainPanelPlayButtons, 0);
        ButtonShow(MainPanelPauseButtons, 1);
        Darker.DOFade(.5f, 1f);
    }
    public void Button_Continue()
    {
        ResetMenu();
    }
    public void Button_BackToGame()
    {
        LeavePanel.SetActive(false);
    }
    public void Button_Leave()
    {
        LeavePanel.SetActive(true);
    }
    public void Button_SecondLeave()
    {
        var gameObjects = GameObject.FindGameObjectsWithTag("Hand");
        for (var i = 0; i < gameObjects.Length; i++)
            Destroy(gameObjects[i]);
        gameObjects = GameObject.FindGameObjectsWithTag("HandShadow");
        for (var i = 0; i < gameObjects.Length; i++)
            Destroy(gameObjects[i]);
        
        MathGameController.instance.ResetScoreAndTimer();
        GameplayMode.Set(GamePlayModes.None);
        GameUI.SetActive(false);
        Game.SetActive(false);
        Invoke(nameof(DisableGame), 2f);
        ButtonShow(MainPanelFirstScreenButtons, 1);
        ButtonShow(MainPanelPlayButtons, 0);
        ButtonShow(MainPanelPauseButtons, 0);
        LeavePanel.SetActive(false);
        ClearMenuElements();
        Background.DOFade(1f, 1f);
    }
    
    public void FinishGame()
    {
        ButtonShow(MainPanelFirstScreenButtons, 1);
        ButtonShow(MainPanelPlayButtons, 0);
        ButtonShow(MainPanelPauseButtons, 0);
        
        ClearMenuElements();
        EndUI.SetActive(true);
        LeavePanel.SetActive(false);
        MainMenuUI.SetActive(false);
    }
    public void Button_Challenge()
    {
        GameplayMode.Set(GamePlayModes.Challenge);
        CheckUserPhoto();
        ResetMenu();
        Invoke("Invoke_AISync",0.4f);
        soundController.PlayConfirm();
    }
    public void Button_Practice()
    {
        GameplayMode.Set(GamePlayModes.Practice);
        
        if (ButtonTimer == 0f) ResetMenu();

        Invoke("Invoke_AISync",0.4f);
        soundController.PlayConfirm();
    }
    public void Button_Options()
    {
        SetMenuElements("Options");
        soundController.GetSliderValues();
        usernamePreviewOptions.text = DBManager.instance.username;
        usernamePreviewInKeyboardOptions.text = DBManager.instance.username;
        OptionsMenu.SetActive(true);
    }
    public void Button_Gloves()
    {
        SetMenuElements("Gloves");
        if (PlayerPrefs.HasKey("GloveIndex")) SelectedGloveIndex = PlayerPrefs.GetInt("GloveIndex");
        GlovesMenu.SetActive(true);
        glovesMenuScript.SelectGlove(SelectedGloveIndex);
    }
    private void Button_Skill()
    {
        SetMenuElements("Select 2 Skills");
        SkillMenu.SetActive(true);
        skillMenuScript.SkillMenuSet();
    }

    public void StartGamePlay()
    {
        Game.SetActive(true);
        GameUI.SetActive(true);
        soundController.FadeOutMainMenuSound();
        soundController.FadeInAmbient();
        Background.DOFade(0f, 1f);
        Darker.DOFade(0f, 1f);
        Invoke(nameof(Invoke_DeActivate_MainMenu), 1f);
    }
    public void ExtraTime()
    {
        StartGamePlay();
        ChallengeController.instance.isExtraTime = true;
        countDownTimer.gameObject.SetActive(true);
        countDownTimer.StartTimer(); 
    }
    public void BackToMainMenu()
    {
        OnEnableEvent();
        Button_Back();
    }
    private void Invoke_DeActivate_MainMenu()
    {
        if(gameObject != null) gameObject.SetActive(false);
    }
    private void CloseAllSubMenus()
    {
        OptionsMenu.SetActive(false);
        GlovesMenu.SetActive(false);
        SkillMenu.SetActive(false);
    }
    private void UpdateUserProfile()
    {
        Texture2D tex;
        GameObject.Find("Text _ UserName").GetComponent<TMP_Text>().text = DBManager.instance.username;
        if (AuthUser.photo != null)
        {
            tex = AuthUser.photo;
            GameObject.Find("User Icon").GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0f, 0f));
        }
    }

    private void CheckUserPhoto()
    {
        if (AuthUser.photo != null)
        {
            Texture2D tex;
            tex = AuthUser.photo;
            userImage.GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0f, 0f));
        }
    }

    private void Close_LoadingCanvas()
    {
        var LoadingCanvas = GameObject.Find("Loading Canvas");
        if (LoadingCanvas == null) return;

        DestroyImmediate(LoadingCanvas.transform.GetComponentsInChildren<RectTransform>().ToList().Find(x => x.name == "Background").gameObject);
        RectTransform[] rts = LoadingCanvas.transform.GetComponentsInChildren<RectTransform>();

        foreach(var v in rts)
        {
            v.DOScale(0f, 1.2f).SetEase(Ease.InOutSine);
        }
        Invoke(nameof(Destroy_LoadingCanvas), 1.5f);
    }
    private void Destroy_LoadingCanvas()
    {
        Destroy(GameObject.Find("Loading Canvas"));
    }

    private void DisableGame()
    {
        Game.SetActive(false);
    }
}