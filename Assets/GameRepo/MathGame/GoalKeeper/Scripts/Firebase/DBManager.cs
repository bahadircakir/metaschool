using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using UnityEngine;

public class DBManager : MonoBehaviour
{
    public static DBManager instance;
    public DatabaseReference usersRef;
    private string userId;
    public List<int> selectedSkills;
    public List<int> numberOfSkills;
    public string username;
    public int isFirstTimeUsing;
    public string deviceModel;
    public string graphicsDeviceName;
    public string batteryLevel;
    public string avgFPS;

    void Awake()
    {
        instance = this;
        userId = SystemInfo.deviceUniqueIdentifier;
    }
    public void Init()
    {
        StartCoroutine(Initialization());
    }

    private IEnumerator Initialization()
    {
        var task = FirebaseApp.CheckAndFixDependenciesAsync();
        while (!task.IsCompleted)
        {
            yield return null;
        }

        if (task.IsCanceled || task.IsFaulted)
        {
            Debug.LogError("Database Error: "+ task.Exception);
        }

        var dependencyStatus = task.Result;

        if (dependencyStatus == DependencyStatus.Available)
        {
            usersRef = FirebaseDatabase.DefaultInstance.GetReference("Users");
            Debug.Log("init completed");
            GetUser();
        }
        else
        {
            Debug.LogError("DB error: ");
        }
    }

    public void AddUser()
    {
        var skillInfo = new Dictionary<string, object>();
        skillInfo["selectedSkills"] = new int[]{0,0,0,0,0,0};
        skillInfo["numberOfSkills"] = new int[]{1,1,1,0,0,0};
        selectedSkills = new List<int>{0,0,0,0,0,0};
        numberOfSkills = new List<int>{1,1,1,0,0,0};
        isFirstTimeUsing = 1;
        deviceModel = SystemInfo.deviceModel;
        graphicsDeviceName = SystemInfo.graphicsDeviceName;
        batteryLevel = SystemInfo.batteryLevel.ToString();
        avgFPS = "60";
        username = "";
        usersRef
            .Child(userId)
            .Child("skillsInfo")
            .SetValueAsync(skillInfo);
        
        usersRef
            .Child(userId)
            .Child("username")
            .SetValueAsync(username);
        
        usersRef
            .Child(userId)
            .Child("isFirstTimeUsing")
            .SetValueAsync(isFirstTimeUsing);
        
        usersRef
            .Child(userId)
            .Child("deviceModel")
            .SetValueAsync(deviceModel);
        
        usersRef
            .Child(userId)
            .Child("graphicsDeviceName")
            .SetValueAsync(graphicsDeviceName);

        usersRef
            .Child(userId)
            .Child("batteryLevel")
            .SetValueAsync(batteryLevel);

        usersRef
            .Child(userId)
            .Child("avgFPS")
            .SetValueAsync(avgFPS);
        
        Debug.Log("New User Created");
    }

    public void GetUser()
    {
        usersRef
            .Child(userId)
            .GetValueAsync().ContinueWithOnMainThread(task => {
                if (task.IsFaulted)
                {
                    Debug.Log("Fault");
                }
                if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    if (snapshot.Value == null)
                    {
                        AddUser();
                    }
                    else
                    {
                        var skillInfo = snapshot.Child("skillsInfo").Value as Dictionary<string, object>;
                        if (skillInfo != null)
                        {
                            var selected = skillInfo["selectedSkills"] as List<object>;
                            var number = skillInfo["numberOfSkills"] as List<object>;
                            selectedSkills = selected.Select(s => Convert.ToInt32(s)).ToList();
                            numberOfSkills = number.Select(s => Convert.ToInt32(s)).ToList();
                        }
                    }
                }
            });
    }
    public void UpdateUsername()
    {
        var childUpdates = new Dictionary<string, object>();
        childUpdates.Add("username", username);
        usersRef
            .Child(userId)
            .UpdateChildrenAsync(childUpdates);
    }
    
    public void UpdateIsFirstTimeUsing()
    {
        var childUpdates = new Dictionary<string, object>();
        childUpdates.Add("isFirstTimeUsing", isFirstTimeUsing);
        usersRef
            .Child(userId)
            .UpdateChildrenAsync(childUpdates);
    }

    public void UpdateNumberOfSkills()
    {
        var childUpdates = new Dictionary<string, object>();
        childUpdates.Add("numberOfSkills", numberOfSkills);
        usersRef
            .Child(userId)
            .Child("skillsInfo")
            .UpdateChildrenAsync(childUpdates);
    }
    
    public void UpdateSelectedSkills()
    {
        var childUpdates = new Dictionary<string, object>();
        childUpdates.Add("selectedSkills", selectedSkills);
        usersRef
            .Child(userId)
            .Child("skillsInfo")
            .UpdateChildrenAsync(childUpdates);
    }
    
    public void UpdateFPS()
    {
        var childUpdates = new Dictionary<string, object>();
        childUpdates.Add("avgFPS", avgFPS);
        usersRef
            .Child(userId)
            .UpdateChildrenAsync(childUpdates);
    }

    public void UpdateBatteryLevel()
    {
        var childUpdates = new Dictionary<string, object>();
        childUpdates.Add("batteryLevel", batteryLevel);
        usersRef
            .Child(userId)
            .UpdateChildrenAsync(childUpdates);
    }
}
