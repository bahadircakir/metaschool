using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using TMPro;
using UnityEngine;
using DG.Tweening;
using Random = UnityEngine.Random;

public class ReactionParticleController : MonoBehaviour
{
    public static ReactionParticleController Instance;
    public List<TextMeshProUGUI> Texts;
    public List<Color> GoodTextColors;
    public List<Color> BadTextColors;
    public List<ParticleSystem> SaveParticles;
    public List<ParticleSystem> GoalParticles;
    public List<ParticleSystem> ComboParticles;

    private List<string> Text_SaveReactions = new List<string>() { "Good!", "Awesome!", "Excellent!", "Great!", "Fantastic!", "Wonderful!"};
    private List<string> Text_GoalReactions = new List<string>() { "OMG!", "Bad!", "Hell!", "Damn It!" };
    void Awake()
    {
        Instance = this;
    }

    public void PlaySave()
    {
        ParticleSystem p = SaveParticles[Random.Range(0, SaveParticles.Count)];
        p.Play();
    }
    public void PlayGoal()
    {
        ParticleSystem p = GoalParticles[Random.Range(0, GoalParticles.Count)];
        p.Play();
    }

    public void PlayCombo(UnityEngine.Vector3 pos)
    {
        ParticleSystem p = ComboParticles[Random.Range(0, ComboParticles.Count)];
        p.Play();
    }
    public void PlayText_Save(string text = "")
    {
        if (text == "") text = Text_SaveReactions[Random.Range(0, Text_SaveReactions.Count)];

        TextMeshProUGUI tm = Texts[Random.Range(0, Texts.Count)];
        tm.SetText(text);
        tm.color = GoodTextColors[Random.Range(0, GoodTextColors.Count)];
        RectTransform rt = tm.gameObject.GetComponent<RectTransform>();

        rt.DOScale(1f, .3f).OnComplete(() => rt.DOScale(0f, 1.6f)).SetEase(Ease.InSine);

        rt.DOShakeAnchorPos(1f, 30f, 6);

    }
    public void PlayText_Goal(string text = "")
    {
        if (text == "") text = Text_GoalReactions[Random.Range(0, Text_GoalReactions.Count)];

        TextMeshProUGUI tm = Texts[Random.Range(0, Texts.Count)];
        tm.SetText(text);
        tm.color= BadTextColors[Random.Range(0, BadTextColors.Count)];
        RectTransform rt = tm.gameObject.GetComponent<RectTransform>();

        rt.DOScale(1f, .3f).OnComplete(() => rt.DOScale(0f, 1.6f)).SetEase(Ease.InSine);

        rt.DOShakeAnchorPos(1f,30f, 6);
    }
    
    public void PlayText_Combo()
    {
        var text = String.Concat("Combo: ", ChallengeController.instance.comboCount);

        TextMeshProUGUI tm = Texts[Random.Range(0, Texts.Count)];
        tm.SetText(text);
        tm.color= GoodTextColors[Random.Range(0, GoodTextColors.Count)];
        RectTransform rt = tm.gameObject.GetComponent<RectTransform>();
        //PlayCombo(rt.position);
        rt.DOScale(1f, .3f).OnComplete(() => rt.DOScale(0f, 1.6f)).SetEase(Ease.InSine);

        rt.DOShakeAnchorPos(1f,30f, 6);
        
    }
}
