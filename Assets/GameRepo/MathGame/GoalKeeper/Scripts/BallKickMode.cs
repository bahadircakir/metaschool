public static class BallKickMode
{
    private static BallKickModes Mode = BallKickModes.Easy;
    public static BallKickModes Get() { return Mode; }
    public static void Set(BallKickModes ballKickMode) { Mode = ballKickMode; }
}

public enum BallKickModes
{
    Easy,
    Medium,
    Hard
}