using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;

public class SkillManager : MonoBehaviour
{
    public static SkillManager instance;
    
    public Skill[] skills;
    public GameObject left_hand;
    public GameObject right_hand;
    public ResponsiveScaler goalScaler;
    private float time = 10f;

    private float xPercentage;
    private float yPercentage;
    
    private GameObject[] colliders;
    private GameObject[] panels;
    public GameObject skillActionTutorialUI;
    
    public GameObject BiggerBallCollider;
    public GameObject SlowerBallCollider;
    public GameObject SmallerGoalCollider;
    public GameObject BiggerGloveCollider;
    public GameObject X2ScoreCollider;
    public GameObject ShowTargetCollider;
    
    public GameObject BiggerBallPanel;
    public GameObject SlowerBallPanel;
    public GameObject SmallerGoalPanel;
    public GameObject BiggerGlovePanel;
    public GameObject X2ScorePanel;
    public GameObject ShowTargetPanel;

    public GameObject background1;
    public GameObject background2;

    private int selected_skill1 = -1;
    private int selected_skill2 = -1;

    private bool initialize = false;
    private bool disabled = false;

    public bool GetSkillFromCombo = false;

    private void Start()
    {
        instance = this;
        panels = new GameObject[6];
        colliders = new GameObject[6];

        panels[0] = BiggerBallPanel;
        panels[1] = SmallerGoalPanel;
        panels[2] = BiggerGlovePanel;
        panels[3] = SlowerBallPanel;
        panels[4] = X2ScorePanel;
        panels[5] = ShowTargetPanel;
        
        colliders[0] = BiggerBallCollider;
        colliders[1] = SmallerGoalCollider;
        colliders[2] = BiggerGloveCollider;
        colliders[3] = SlowerBallCollider;
        colliders[4] = X2ScoreCollider;
        colliders[5] = ShowTargetCollider;

        initialize = true;
    }

    public void UpdateSkills() // change to OnEnable after pause menu skills delete
    {
        if (!initialize) return;
        if (MathGameController.instance.CheckGameStarted()) return;

        if (SkillDatabase.GetSelectedSkills().Count == 0) return;

        var x1 = GoalController.instance.topRight.position.x + 0.25f;
        var x2 = GoalController.instance.topRight.position.x + 0.15f;
        var x3 = GoalController.instance.topRight.position.x + 0.1f;
        var y1 = (GoalController.instance.topRight.position.y + GoalController.instance.bottomLeft.position.y) * 0.92f;
        var y2 = (GoalController.instance.topRight.position.y + GoalController.instance.bottomLeft.position.y) * 0.1f;
        var y3 = (GoalController.instance.topRight.position.y + GoalController.instance.bottomLeft.position.y) * 0.5f;
        var z = GoalController.instance.topRight.position.z - GoalController.instance.thickness / 2;
        
        if (SkillDatabase.GetSelectedSkills().Count == 1)
        {
            selected_skill1 = (int)SkillDatabase.GetSelectedSkills()[0];
            if (GameplayMode.Get() == GamePlayModes.Challenge)
            {  
                background1.SetActive(true);
                colliders[selected_skill1].SetActive(true);
                colliders[selected_skill1].transform.position = new Vector3(x3, y3, z);
                
                panels[selected_skill1].SetActive(true);
                panels[selected_skill1].GetComponent<RectTransform>().anchoredPosition = new Vector2(47f, 0f);
            }
        }
        else if (SkillDatabase.GetSelectedSkills().Count == 2)
        {
            selected_skill1 = (int)SkillDatabase.GetSelectedSkills()[0];
            selected_skill2 = (int)SkillDatabase.GetSelectedSkills()[1];

            if (GameplayMode.Get() == GamePlayModes.Challenge)
            {
                background2.SetActive(true);
                colliders[selected_skill1].SetActive(true);
                colliders[selected_skill2].SetActive(true);
            
                colliders[selected_skill1].transform.position = new Vector3(x3, y1, z);
                colliders[selected_skill2].transform.position = new Vector3(x3, y2, z);
                
                panels[selected_skill1].SetActive(true);
                panels[selected_skill2].SetActive(true);
            
                panels[selected_skill1].GetComponent<RectTransform>().anchoredPosition = new Vector2(47f, 250f);
                panels[selected_skill2].GetComponent<RectTransform>().anchoredPosition = new Vector2(47f, -250f);
            }
        }
    }

    private void OnEnable()
    {
        disabled = false;
    }

    private void OnDisable()
    {
        if (time < 10.0f)
            goalScaler.Scale(xPercentage, yPercentage);
        
        background1.SetActive(false);
        background2.SetActive(false);
        
        if (selected_skill1 != -1)
        {
            colliders[selected_skill1].SetActive(false);
            panels[selected_skill1].SetActive(false);
        }

        if (selected_skill2 != -1)
        {
            colliders[selected_skill2].SetActive(false);
            panels[selected_skill2].SetActive(false);
        }
        
        selected_skill1 = -1;
        selected_skill2 = -1;
        
        GetSkillFromCombo = false;
    }

    public bool GamePaused()
    {
        return MathGameController.instance.CheckGamePaused();
    }

    public bool isSkillActive(string name)
    {
        foreach (var item in skills)
        {
            if (item.name == name)
                return item.isActive;
            
        }
        return false;
    }
    
    public void setSkillActive(string name)
    {
        foreach (var item in skills)
        {
            if (item.name == name)
                item.setIsActive(true);
        }
    }
    
    public void disableSkill(string name)
    {
        foreach (var item in skills)
        {
            if (item.name == name)
                item.setIsActive(false);
        }
    }

    public void DoBiggerGloves()
    {
        Vector3 scaleLocal = left_hand.transform.localScale;
        left_hand.transform.localScale *= 1.5f;
        right_hand.transform.localScale *= 1.5f;
        StartCoroutine(ReChangeGlove(10f, scaleLocal));
    }

    public void DoSmallerGoalPost()
    {
        xPercentage = goalScaler.xScaleReductionPercentage;
        yPercentage = goalScaler.yScaleReductionPercentage;
        goalScaler.Scale(xPercentage + (1 - xPercentage) * 0.1f, yPercentage + (1 - yPercentage) * 0.15f);
        StartCoroutine(ReChangeGoal());
    }
    
    private IEnumerator ReChangeGlove(float time, Vector3 scale)
    {
        while (time > 0)
        {
            if (disabled)
                break;
            while (GamePaused())
            {
                yield return new WaitForSeconds(0.5f);
            }
            yield return new WaitForSeconds(1);
            time--;
        }
        left_hand.transform.localScale = scale;
        right_hand.transform.localScale = scale;
    }
    
    private IEnumerator ReChangeGoal()
    {
        time = 10f;
        while (time > 0)
        {
            if (disabled)
            {
                break;
                
            }

            while (GamePaused())
            {
                yield return new WaitForSeconds(0.5f);
            }
            yield return new WaitForSeconds(1);
            time--;
        }
        goalScaler.Scale(xPercentage, yPercentage);
    }
}
