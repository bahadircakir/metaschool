using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class BiggerGloveController : MonoBehaviour
{
    public Image slider;
    public RawImage image;
    public Image background;
    public TMP_Text timerText;

    private SkillManager skillManager;
    private bool triggerEntered = false;
    private bool triggerExited = false;
    private const int TIME_TO_ENABLE_SKILL = 1;
    public int SKILL_TIME = 10;
    private bool state = false;
    private bool used = false;
    private float timer = 0f;
    private bool isCoroutineActive = false;

    private void Start()
    {
        skillManager = FindObjectOfType<SkillManager>();
    }
    
    private void Update()
    {
        if (triggerEntered)
        {
            timer += Time.deltaTime;
            Fill();
        } else if (triggerExited)
        {
            timer += Time.deltaTime;
            UnFill();
        }
        
        if (SkillManager.instance.GetSkillFromCombo)
        {
            if (used)
            {
                used = false;
                Color color = image.color;
                color.a = 1.0f;
                image.color = color;
            }
            
            SkillManager.instance.GetSkillFromCombo = false;
        }
    }
    
    private void OnEnable()
    {
        slider.fillAmount = 0;
        timerText.text = "";
        background.fillAmount = 1.0f;
        
        triggerEntered = false;
        triggerExited = false;
    }

    private void OnDisable()
    {
        if(isCoroutineActive)
            StopCoroutine(CountdownCoroutine());
        ReturnToInit(1);
        isCoroutineActive = false;
    }

    private void ReturnToInit(int type)
    {
        timerText.text = "";
        SKILL_TIME = 10;
        slider.fillAmount = 0.0f;
    
        background.fillAmount = 1.0f;
        Color color = background.color;
        color.a = 0f;
        background.color = color;

        image.gameObject.SetActive(true);
        color = image.color;
        color.a = 0.4f;
        image.color = color;
        
        skillManager.disableSkill("BiggerGlove");           
        
        if (type == 1)
        {
            used = false;
            
            color = image.color;
            color.a = 1.0f;
            image.color = color;
        }
    }

    private void HandleSkill()
    {
        state = false;
        slider.fillAmount = 0f;
        Color color = background.color;
        color.a = 1.0f;
        background.color = color;

        image.gameObject.SetActive(false);

        timerText.text= "" + SKILL_TIME;

        if (!isCoroutineActive)
        {
            skillManager.DoBiggerGloves();
            StartCoroutine(CountdownCoroutine());
        }
    }
    
    IEnumerator CountdownCoroutine()
    {
        if (!PlayerPrefs.HasKey("SkillActionTutorial"))
        {
            PlayerPrefs.SetInt("SkillActionTutorial", 0);
            SkillManager.instance.skillActionTutorialUI.SetActive(false);
            MathGameController.instance.StartGame();
        }
        isCoroutineActive = true;
        yield return new WaitForSeconds(0.5f);
        while (SKILL_TIME > 1)
        {
            while (skillManager.GamePaused())
            {
                yield return new WaitForSeconds(0.5f);
            }
            SKILL_TIME -= 1;
            timerText.text = "" + SKILL_TIME;
            background.fillAmount = SKILL_TIME / 10.0f;
            yield return new WaitForSeconds(1);
        }

        ReturnToInit(0);
        isCoroutineActive = false;
    }



    private void OnTriggerEnter(Collider collisionInfo)
    {
        if (used) return;
        if (!collisionInfo.CompareTag("Hand")) return;
        if (!state && !skillManager.isSkillActive("BiggerGlove"))
        {
            timer = 0;
            state = true;
            triggerEntered = true;
            triggerExited = false;
        }
    }

    private void Fill()
    {
        if (timer >= 0.05f)
        {
            timer = 0;
            slider.fillAmount += 1.0f/(TIME_TO_ENABLE_SKILL*20);   
            if (slider.fillAmount >= 1.0f)
            {
                skillManager.setSkillActive("BiggerGlove");
                used = true;
                SkillDatabase.DecreaseSkillSize(Skills.BiggerGloves, 1);
                HandleSkill();
                triggerEntered = false;
            }
        }
    }

    private void UnFill()
    {
        if (timer >= 0.05f)
        {
            timer = 0;
            slider.fillAmount -= 1.0f/(TIME_TO_ENABLE_SKILL*20);
            if (slider.fillAmount <= 0.0f)
            {
                triggerExited = false;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Hand")) return;
        if (state && !skillManager.isSkillActive("BiggerGlove"))
        {
            timer = 0;
            state = false;
            triggerEntered = false;
            triggerExited = true;
        }
    }
}
