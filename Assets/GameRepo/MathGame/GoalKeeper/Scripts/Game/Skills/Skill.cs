using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Skill
{
    public string name;
    public bool isActive = false;
    
    public void setIsActive(bool val)
    {
        isActive = val;
    }
}