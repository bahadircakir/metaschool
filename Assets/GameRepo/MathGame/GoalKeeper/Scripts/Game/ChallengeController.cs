using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class ChallengeController : MonoBehaviour
{
    public static ChallengeController instance;
    public TMP_Text scoreText;
    public TMP_Text timerText;
    public IEnumerator EndGameAction;
    public bool ChallengeModeIsRun;
    public bool isExtraTime;
    public int currentScore;
    public int comboCount;
    public float avgFPS;
    public int totalBallsCount;
    public int savedBallsCount;
    private void Awake()
    {
        instance = this;
        isExtraTime = false;
        comboCount = 0;
        totalBallsCount = 0;
        savedBallsCount = 0;
    }
    public void StartChallengeMode(IEnumerator endGame)
    {
        EndGameAction = endGame;
        ChallengeModeIsRun = true;
        StartCoroutine(Challenge_Mode()); 
        StartCoroutine(ChallengeTimer(90));
    }
    public void StartExtraGame()
    {
        ChallengeModeIsRun = true;
        StartCoroutine(ExtraTimeGame());
    }
    public void EndGame()
    {
        isExtraTime = false;
        ChallengeModeIsRun = false;
        currentScore = 0;
        totalBallsCount = 0;
        savedBallsCount = 0;
        timerText.text = "Time: 90";
        scoreText.text = "Score: 0";
        timerText.gameObject.SetActive(false);
        scoreText.gameObject.SetActive(false);
    }
    
    private IEnumerator setMode(BallKickModes mode, int modeCoef)
    {
        BallKickMode.Set(mode);
        for (var i = 0; i < modeCoef; i++)
        {
            while (!ChallengeModeIsRun)
            {
                yield return new WaitForSeconds(0.5f);
            }
            yield return new WaitForSeconds(1.0f);
        }
    }
    private IEnumerator Challenge_Mode()
    {
        GetReadyForChallengeMode();
        yield return StartCoroutine(setMode(BallKickModes.Easy,20));
        yield return StartCoroutine(setMode(BallKickModes.Medium,25));
        MathGameController.instance.referee2.GetComponent<HakemGAI>().State = GAIState.Start;
        yield return StartCoroutine(setMode(BallKickModes.Easy,20));
        yield return StartCoroutine(setMode(BallKickModes.Medium,25));
        ChallengeFinishUpdate();
    }

    private IEnumerator ExtraTimeGame()
    {
        StartCoroutine(ChallengeTimer(21));
        MathGameController.instance.referee.GetComponent<HakemGAI>().State = GAIState.Start;
        MathGameController.instance.referee2.GetComponent<HakemGAI>().State = GAIState.Start;
        yield return StartCoroutine(setMode(BallKickModes.Medium,21));
        ChallengeFinishUpdate();
    }
    
    private void GetReadyForChallengeMode()
    {
        avgFPS = 0;
        timerText.gameObject.SetActive(true);
        scoreText.gameObject.SetActive(true);
    }
    private IEnumerator ChallengeTimer(int seconds)
    {
        while (seconds > 0)
        {
            seconds--;
            timerText.text = String.Concat("Time: ",seconds.ToString());
            yield return new WaitForSeconds(1.0f);
            while (!ChallengeModeIsRun)
            {
                yield return new WaitForSeconds(0.5f);
            }
        }
        Debug.Log("ChallengeTimer");
    }
    
    public void AddScore(int score)
    {
        if (SkillManager.instance.isSkillActive("X2Score"))
            currentScore += score * 2;
        else
            currentScore += score;
        scoreText.text = String.Concat("Score: ", currentScore);
    }
    
    public async void ChallengeFinishUpdate()
    {
        UpdateAvgFPS();
        StartCoroutine(EndGameAction);
        MainMenuController.instance.FinishGame();
    }
    
    private void UpdateAvgFPS()
    {
        DBManager.instance.avgFPS = avgFPS.ToString();
        DBManager.instance.UpdateFPS();
    }
    
}