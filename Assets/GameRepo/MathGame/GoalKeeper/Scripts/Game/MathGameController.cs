using System.Collections;
using UnityEngine;

public class MathGameController : MonoBehaviour
{
    public static MathGameController instance;
    private MainMenuController _mainMenuController;
    [SerializeField] private GameObject _mainMenu;
    public GameObject referee;
    public GameObject referee2;
    private GameState gameState = GameState.None;

    private void Awake()
    {
        instance = this;
        _mainMenuController = _mainMenu.GetComponent<MainMenuController>();
    }

    public void StartGame()
    {
        StartReferees();
        if (GameplayMode.Get() == GamePlayModes.Practice)
            PracticeController.instance.StartPracticeMode();
        if (GameplayMode.Get() == GamePlayModes.Challenge)
            ChallengeController.instance.StartChallengeMode(EndGame());
        GameSetState(GameState.Run);
    }

    private IEnumerator EndGame()
    {
        referee.GetComponent<HakemGAI>().State = GAIState.Stop;
        referee2.GetComponent<HakemGAI>().State = GAIState.Stop;
        yield return new WaitForSeconds(0.5f);
        GameSetState(GameState.Finished);
    }

    public void PauseGame()
    {
        PauseReferees();
        GameSetState(GameState.Paused);
    }    
    public void ContinueGame()
     {
         StartReferees();
         GameSetState(GameState.Run);
     }
     public void StartReferees()
     {
         referee.GetComponent<HakemGAI>().State = GAIState.Start;
         if (referee2.GetComponent<HakemGAI>().State == GAIState.Pause)
         {
             referee2.GetComponent<HakemGAI>().State = GAIState.Start;
         }
     }
     public void PauseReferees()
     {
         referee.GetComponent<HakemGAI>().State = GAIState.Pause;
         if (referee2.GetComponent<HakemGAI>().State == GAIState.Start)
         {
             referee2.GetComponent<HakemGAI>().State = GAIState.Pause;
         }
     }
     private void GameSetState(GameState state)
     {
         gameState = state;
         if (GameplayMode.Get() == GamePlayModes.Practice)
             PracticeController.instance.PracticeModeIsRun = (gameState == GameState.Run);

         else if (GameplayMode.Get() == GamePlayModes.Challenge)
             ChallengeController.instance.ChallengeModeIsRun = (gameState == GameState.Run);
     }
     public void ResetScoreAndTimer()
     {
         StartCoroutine(EndGame());
         ChallengeController.instance.EndGame();
     }
     public bool CheckGameStarted()
     {
         return gameState == GameState.Run;
     }
    
     public bool CheckGamePaused()
     {
         return gameState == GameState.Paused;
     }    
     public bool CheckGameFinished()
     {
         return gameState == GameState.Finished;
     }
}
public enum GameState
{
    None,
    Run,
    Paused,
    Finished
}