using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalController : MonoBehaviour
{
    [HideInInspector] public Vector3 center;
    [HideInInspector] public Vector3 radius;
    [HideInInspector] public Transform bottomLeft;
    [HideInInspector] public Transform topRight;
    [HideInInspector] public float upperRestrictive;
    [HideInInspector] public Vector3 bottomCenter;
    [HideInInspector] public float thickness;
    public static GoalController instance;
    
    void Start()
    {
        instance = this;
        thickness = GetComponent<Collider>().bounds.size.z;
        Transform goalBottomLeft = transform.Find("BottomLeft");
        Transform goalTopRight = transform.Find("TopRight");
        bottomLeft = goalBottomLeft;
        topRight = goalTopRight;
        center = new Vector3((goalTopRight.position.x + goalBottomLeft.position.x) / 2, (goalTopRight.position.y + goalBottomLeft.position.y) / 2 * 1.1f, goalTopRight.position.z);
        radius = center - new Vector3(goalTopRight.position.x, (goalTopRight.position.y + goalBottomLeft.position.y) / 2 * 1.1f, goalTopRight.position.z);
        upperRestrictive = goalTopRight.position.y;
        bottomCenter = (goalBottomLeft.position + new Vector3(goalTopRight.position.x, goalBottomLeft.position.y, goalBottomLeft.position.z)) / 2;
    }
}
