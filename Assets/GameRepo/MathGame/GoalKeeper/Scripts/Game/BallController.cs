using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class BallController : MonoBehaviour
{
    public GameObject goalEffect;
    public GameObject shadowProjectorPrefab;
    public GameObject targetPrefab;
    public float airDensity;
    public float gravityScale;
    public GameObject Particle;
    [HideInInspector] public bool effectCreated;
    [HideInInspector] public FootballPlayerGAI footballPlayer;

    private float verticalTorqueScaler = 2;
    private float horizontalTorqueScaler = 5;
    private float verticalTorque;
    private float horizontalTorque;
    private float force;
    private static float globalGravity = -9.81f;
    private Vector3 gravity;
    private Vector3 estimatedTarget;
    private float distance;
    private float distancePercentage = 1;
    private bool dragIncreased;
    private float dragScaler;
    private float angularDragScaler;
    private float radius;
    private Rigidbody rb;
    private GameObject target_skill;
    private GameObject goal;
    private GameObject shadowProjector;
    private GameObject target;
    private Collider targetAreaCollider;
    private SkillManager skillManager;
    private Transform projector;
    private bool isHit;
    private bool isGoal;
    private bool isMiss;
    private bool isPerfectSave;
    private float perfectSaveTimer;
    private bool ready;

    private void Awake()
    {
        target = Instantiate(targetPrefab);
        target.SetActive(false);
        rb = gameObject.GetComponent<Rigidbody>();
        goal = GameObject.FindWithTag("Goal");
        targetAreaCollider = goal.transform.Find("BallTargetArea").GetComponent<Collider>();
        skillManager = FindObjectOfType<SkillManager>();
        target_skill = GameObject.FindWithTag("Target");
        
        if(shadowProjectorPrefab != null)
        {
            shadowProjector = Instantiate(shadowProjectorPrefab);
            shadowProjector.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        }
        
        radius = GetComponent<Collider>().bounds.size.y / 2;
        gravity = globalGravity * gravityScale * Vector3.up;
        if (skillManager.isSkillActive("BiggerBall"))
        {
            transform.localScale *= 1.3f;
            airDensity *= 2f;
            gravity *= 1.2f;
        }
        
        //audioManager = FindObjectOfType<AudioManager>();
        
        dragScaler = 2.5f;
        angularDragScaler = 28f;
        rb.maxAngularVelocity = 500;
        
        Destroy(gameObject, 7);
    }

    public void ThrowBall()
    {
        if(Particle != null)
        {
            Particle.SetActive(true);
        }
        
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        CalculatePhysicsParameters();
        ready = true;
        
        distance = estimatedTarget.z - transform.position.z;
        target.transform.position = estimatedTarget;
        if (skillManager.isSkillActive("SlowerBall"))
        {
            rb.AddForce(transform.forward * 3 * force / 4);
            airDensity *= 0.75f;
            gravity *= 0.6f;
            dragScaler *= 0.75f;
            angularDragScaler *= 0.75f;
        }
        else
        {
            rb.AddForce(transform.forward * force);
        }
        rb.AddTorque(transform.up * horizontalTorque, ForceMode.Force);
        rb.AddTorque(transform.right * verticalTorque, ForceMode.Force);
        if (skillManager.isSkillActive("ShowTarget"))
        {
            target_skill.transform.position = estimatedTarget;
            target_skill.transform.localScale = new Vector3(1.5f, 1.5f, 0);
            StartCoroutine(WaitSec(1.5f));
        }

        Invoke("WaitResponse", 3f);
    }

    private void CalculatePhysicsParameters()
    {
        var targetBounds = targetAreaCollider.bounds;
        var targetBoundsScaler = 1f;
        if (skillManager.isSkillActive("BiggerBall"))
        {
            targetBoundsScaler = 0.8f;
        }

        var xHeight = transform.position.x < targetBounds.center.x ? Random.Range(-0.9f, 1.1f) : Random.Range(-1.1f, 0.9f);
        var yHeight = 0f;
        var choice = Random.Range(1, 7);
        var targetX = Random.Range(transform.position.x < targetBounds.center.x ? targetBounds.min.x : (targetBounds.min.x + targetBounds.max.x) * 0.2f, transform.position.x < targetBounds.center.x ? (targetBounds.min.x + targetBounds.max.x) * 0.8f : targetBounds.max.x);
        var targetY = 0f;
        if (choice == 1 || choice == 2)
        {
            targetY = Random.Range(targetBounds.min.y, targetBounds.max.y * targetBoundsScaler * 1 / 4);
            switch (BallKickMode.Get())
            {
               case BallKickModes.Easy:
                   yHeight = Random.Range(0f, 1f);
                   break;
               case BallKickModes.Medium:
                   yHeight = Random.Range(0f, 0.5f);
                   break;
               case BallKickModes.Hard:
                   yHeight = 0;
                   break;
            }
            verticalTorque = Mathf.Pow(yHeight, 2) * verticalTorqueScaler;
        } else if (choice == 3)
        {
            targetY = Random.Range(targetBounds.max.y * 1 / 4, targetBounds.max.y * targetBoundsScaler * 3 / 4);
            switch (BallKickMode.Get())
            {
                case BallKickModes.Easy:
                    yHeight = Random.Range(0f, 1.5f);
                    break;
                case BallKickModes.Medium:
                    yHeight = Random.Range(0f, 0.75f);
                    break;
                case BallKickModes.Hard:
                    yHeight = 0;
                    break;
            }
            verticalTorque = yHeight * verticalTorqueScaler;
        }
        else
        {
            targetY = Random.Range(targetBounds.max.y * 3 / 4, targetBounds.max.y * targetBoundsScaler);
            switch (BallKickMode.Get())
            {
                case BallKickModes.Easy:
                    yHeight = 0.4f;
                    break;
                case BallKickModes.Medium:
                    yHeight = 0.2f;
                    break;
                case BallKickModes.Hard:
                    yHeight = 0;
                    break;
            }
            verticalTorque = 0;
        }
        var targetZ = targetBounds.max.z;
        Vector3 estimatedTargetPos = new Vector3(targetX, targetY, targetZ);
        Debug.Log(estimatedTargetPos);
        Vector3 ballTargetPoint = CalculateTargetPoint(transform.position, estimatedTargetPos, xHeight, yHeight); 
        transform.LookAt(ballTargetPoint);
        
        switch (BallKickMode.Get())
        {
            case BallKickModes.Easy:
                force = 110;
                break;
            case BallKickModes.Medium:
                force = 125;
                break;
            case BallKickModes.Hard:
                force = 135;
                break;
        }
        estimatedTarget = estimatedTargetPos;
        horizontalTorque = xHeight * horizontalTorqueScaler;
    }
        
    private Vector3 CalculateTargetPoint(Vector3 startPoint, Vector3 endPoint, float xHeight, float yHeight)
    {
        var centerX = (startPoint.x + endPoint.x) / 2;
        var centerY = (startPoint.y + endPoint.y) / 2;
        var centerZ = (startPoint.z + endPoint.z) / 2;
        
        var controlPointX =  (centerX + xHeight - 0.25f * startPoint.x) / 0.75f;
        var controlPointY =  (centerY + yHeight - 0.25f * startPoint.y) / 0.75f;
        var controlPointZ = centerZ;

        return new Vector3(controlPointX, controlPointY, controlPointZ);
    }
    
    private void Update()
    {
        if (!ready || isGoal || isMiss)
        {
            return;
        }
        if (isHit)
        {
            if (isPerfectSave)
            {
                perfectSaveTimer += Time.deltaTime;
            }
        }

        if (distancePercentage < 0)
        {
            if (!CheckIfInsideGoalArea())
            {
                isMiss = true;
                DestroyTarget();
                return;
            }
            isGoal = true;
            ChallengeController.instance.comboCount = 0;
            var effect = Instantiate(goalEffect);
            effect.transform.localRotation = Quaternion.identity;

            Vector3 position = transform.position;
            effect.transform.position = position;
            effect.transform.localScale = Vector3.one;
            DestroyTarget();

            //Apply Sound And Particle--------------------------------------------------
            float pan = 1f;
            if (transform.position.x < 0.5f && transform.position.x > -0.5f) pan = 0f;
            else if (transform.position.x <= -0.5f) pan = -1f;
            SoundController.Instance.PlayBallWind(pan);
            SoundController.Instance.PlayReactSad();
            ReactionParticleController.Instance.PlayGoal();
            ReactionParticleController.Instance.PlayText_Goal();
            //--------------------------------------------------------------------------
        } else if (target != null && distancePercentage < 0.1f)
        {
            target.transform.position = new Vector3(transform.position.x, transform.position.y, target.transform.position.z);
            if (!target.activeSelf)
            {
                target.SetActive(true);
            }
        }
    }

    private void LateUpdate()
    {
        
    }

    private bool CheckIfInsideGoalArea()
    {
        var ballPos = transform.position;
        return ballPos.x > GoalController.instance.bottomLeft.position.x &&
               ballPos.x < GoalController.instance.topRight.position.x &&
               ballPos.y > GoalController.instance.bottomLeft.position.y &&
               ballPos.y < GoalController.instance.topRight.position.y;
    }
    
    void FixedUpdate()
    {
        if (!ready)
        {
            if(shadowProjector != null)
            {
                shadowProjector.transform.position = new Vector3(transform.position.x, transform.position.y + radius, transform.position.z);
            }
            rb.AddForce(gravity, ForceMode.Acceleration);
            return;
        }
        if (isPerfectSave && perfectSaveTimer < 0.5f)
        {
            /*var handsCenter = (MLOutputController.instance.hands[0].GetPosition() + MLOutputController.instance.hands[1].GetPosition()) / 2;
            transform.position = new Vector3(handsCenter.x, handsCenter.y, handsCenter.z + radius);*/
            if (shadowProjector != null)
            {
                shadowProjector.transform.position = new Vector3(transform.position.x, transform.position.y + radius, transform.position.z); ;
            }
            return;
        }
        distancePercentage = (estimatedTarget.z - transform.position.z) / distance;
        if (shadowProjector != null)
        {
            shadowProjector.transform.position = new Vector3(transform.position.x, transform.position.y + radius, transform.position.z); ;
        }
        rb.AddForce(gravity, ForceMode.Acceleration);
        if (!dragIncreased && distancePercentage < 0.6f)
        {
            dragIncreased = true;
            rb.drag *= dragScaler;
            rb.angularDrag *= angularDragScaler;
        }
        var direction = Vector3.Cross(rb.angularVelocity, rb.velocity);
        var magnitude = 4 / 3f * Mathf.PI * airDensity * Mathf.Pow(radius, 3);
        rb.AddForce(magnitude * direction);
    }

    private IEnumerator WaitSec(float time)
    {
        yield return new WaitForSeconds(time);
        target_skill.transform.localScale = new Vector3(0f, 0f, 0f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Hand") && !isHit)
        {
            DestroyTarget();
            /*var handsCenter = (MLOutputController.instance.hands[0].GetPosition() + MLOutputController.instance.hands[1].GetPosition()) / 2;
            if (transform.position.x > handsCenter.x - 0.1f && transform.position.x < handsCenter.x + 0.1f)
            {
                if (transform.position.y > handsCenter.y - 0.1f && transform.position.y < handsCenter.y + 0.1f)
                {
                    isPerfectSave = true;
                    rb.velocity = Vector3.zero;
                    rb.angularVelocity = Vector3.zero;
                }
            }*/
            isHit = true;
            if (!isPerfectSave)
            {
                rb.AddForce(Vector3.forward * 15);
            }

            gravity *= 2;
            StartCoroutine(DelayOneSecAndCheckIfReallySaved());
        } else if (collision.gameObject.CompareTag("Goal"))
        {
            DestroyTarget();
            SoundController.Instance.PlaySoccerBallHit(1f);
            rb.AddForce(Vector3.forward * 30);
            gravity *= 2;
        }

        SendResultToPlayer_Collision(collision.collider.gameObject.tag);
    }

    private IEnumerator DelayOneSecAndCheckIfReallySaved() {
        yield return new WaitForSeconds(0.3f);
        if (!isGoal)
        {
            ChallengeController.instance.comboCount++;
            SoundController.Instance.PlaySaveBall();
            SoundController.Instance.PlayReactSave();
            ReactionParticleController.Instance.PlaySave();
            ReactionParticleController.Instance.PlayText_Save();
            if (ChallengeController.instance.comboCount >= 3)
            {
                if (ChallengeController.instance.comboCount % 3 == 0)
                {
                    /*
                    MLOutputController.instance.hands[0].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    MLOutputController.instance.hands[1].transform.GetChild(0).GetComponent<ParticleSystem>().Play();*/
                }
                else if (ChallengeController.instance.comboCount % 5 == 0)
                    SkillManager.instance.GetSkillFromCombo = true;

                ReactionParticleController.Instance.PlayText_Combo();
                
                ChallengeController.instance.AddScore(ChallengeController.instance.comboCount);
            }
            else
            {
                ChallengeController.instance.AddScore(1);
            }
        }
    }

    private void DestroyTarget()
    {
        Destroy(target);
        /*
        MLOutputController.instance.hands[0].targetTriggered = false;
        MLOutputController.instance.hands[1].targetTriggered = false;*/
    }
    
    private void OnDestroy()
    {
        if(shadowProjector != null)
        {
            Destroy(shadowProjector);
        } 
    }


    //Oyuncular sut cektikten sonra gol olup olmadigi bilgisini asagidaki methodlardan ogrenir.
    private void SendResultToPlayer_Trigger(string tag)
    {
        if (tag != "Goal Trigger" && tag != "Miss Trigger") return;

        if(tag == "Goal Trigger") footballPlayer.ThatsGoal = true;
        else if(tag == "Miss Trigger") footballPlayer.MissGoal = true;
    }
    private void SendResultToPlayer_Collision(string tag)
    {
        if (tag != "Goal" && tag != "Hand") return;

        if (tag == "Hand") footballPlayer.MissGoal = true;
        else Invoke("WaitResponse", 1f);
    }
    private void WaitResponse()
    {
        if (footballPlayer == null) return;

        if (!footballPlayer.MissGoal && !footballPlayer.ThatsGoal) footballPlayer.MissGoal = true;
    }
    //-----------------------------------------------------------------------------------------
}
