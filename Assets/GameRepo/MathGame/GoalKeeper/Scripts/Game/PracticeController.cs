using System.Collections;
using UnityEngine;

public class PracticeController : MonoBehaviour
{
    public static PracticeController instance;
    public bool PracticeModeIsRun;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }
    public void StartPracticeMode()
    {
        PracticeModeIsRun = true;
        StartCoroutine(Practice_Mode());
    }    
    public void OnDisable()
    {
        PracticeModeIsRun = false;
    }
    private IEnumerator Practice_Mode()
    {
        BallKickMode.Set(BallKickModes.Easy);
        for (int i = 0; i < 300; i++)
        {
            while (!PracticeModeIsRun)
            {
                yield return new WaitForSeconds(0.5f);
            }
            yield return new WaitForSeconds(5.0f);
        }
    }
}
