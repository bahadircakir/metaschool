using UnityEngine;

public class ResponsiveScaler : MonoBehaviour
{
    public Canvas canvas;
    public Transform bottomLeft;
    public Transform topRight;
    public float xScaleReductionPercentage;
    public float yScaleReductionPercentage;

    private Vector3 screenBottom;
    private Vector3 screenBottomLeft;
    private Vector3 screenTopRight;
    
    void Awake()
    {
        screenBottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, canvas.planeDistance));
        screenTopRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, canvas.planeDistance));
        Scale(xScaleReductionPercentage, yScaleReductionPercentage);
    }

    public void Scale(float xPercentage, float yPercentage)
    {
        var xScale = (screenTopRight.x - screenBottomLeft.x) / (topRight.position.x - bottomLeft.position.x) * (1 - xPercentage);
        var yScale = (screenTopRight.y - screenBottomLeft.y) / (topRight.position.y - bottomLeft.position.y) * (1 - yPercentage);

        transform.localScale = new Vector3(xScale * transform.localScale.x, transform.localScale.y, yScale * transform.localScale.z);
    }
}

