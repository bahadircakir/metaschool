using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLoader : MonoBehaviour
{
    public GameObject SkillManager;
    
    public void OnEnable()
    {
        LoadGlove();
    }

    void LoadGlove()
    {
        GameObject left;
        GameObject right;
        
        int gloveIndex = PlayerPrefs.GetInt("GloveIndex");
        left = (GameObject)Instantiate(Resources.Load("Gloves/left_" + gloveIndex));
        right = (GameObject)Instantiate(Resources.Load("Gloves/right_" + gloveIndex));

        /*
        Hand[] hands = new Hand[2];
        hands[0] = left.GetComponent<Hand>();
        hands[1] = right.GetComponent<Hand>();
        MLOutputController.instance.hands = hands;
        */
        //HAND ACTIVATION
        SkillManager.GetComponent<SkillManager>().left_hand = left;
        SkillManager.GetComponent<SkillManager>().right_hand = right;
    }
}
