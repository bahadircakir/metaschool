using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenMirrorTutorialPanelAnimSound : MonoBehaviour
{
    public SoundController soundController;

    public void PlayConfirmSound()
    {
        soundController.PlayConfirm();
    }
}
