public static class GameplayMode
{
    private static GamePlayModes Mode = GamePlayModes.None;
    public static GamePlayModes Get() { return Mode; }
    public static void Set(GamePlayModes GamePlayMode) { Mode = GamePlayMode; }
}

public enum GamePlayModes
{
    None,
    Practice,
    Challenge
}
