using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SkillDatabase 
{
    public static List<Skills> GetSelectedSkills()
    {
        List<Skills> skills = new List<Skills>();
        for(int i = 0; i < DBManager.instance.selectedSkills.Count; i++)
        {
            if (DBManager.instance.selectedSkills[i] == 1) skills.Add((Skills)i);
        }
        return skills;
    }
    
    public static void SelectSkill(Skills skill)
    {
        DBManager.instance.selectedSkills[(int)skill] = 1;
        if (DBManager.instance.isFirstTimeUsing == 1 && CheckSelectedFirstTwo())
            SkillTutorial.instance.first2SkillsSelected = true;
        else if (DBManager.instance.isFirstTimeUsing == 1 && skill == Skills.BiggerGloves)
            SkillTutorial.instance.thirdSkillSelected = true;
        DBManager.instance.UpdateSelectedSkills();
    }
    
    
    public static void DeselectSkill(Skills skill)
    {
        DBManager.instance.selectedSkills[(int)skill] = 0;
        if (DBManager.instance.isFirstTimeUsing == 1 && skill == Skills.SmallGoal)
            SkillTutorial.instance.secondSkillDeselected = true;
        DBManager.instance.UpdateSelectedSkills();
    }
    
    public static bool IsSelected(Skills skill) 
    {
        if (DBManager.instance.selectedSkills[(int)skill] == 0) return false; 
        return true;
    }

    public static int GetSkillSize(Skills skill)
    {
        return DBManager.instance.numberOfSkills[(int)skill];
    }
    
    public static void IncreaseSkillSize(Skills skill, int numberOfIncrease)
    {
        int size = DBManager.instance.numberOfSkills[(int)skill];
        size += Mathf.Abs(numberOfIncrease);
        DBManager.instance.numberOfSkills[(int)skill] = size;
        DBManager.instance.UpdateNumberOfSkills();
    }
    
    public static void DecreaseSkillSize(Skills skill, int numberOfDecrease)
    {
        int size = DBManager.instance.numberOfSkills[(int)skill];
        size -= Mathf.Abs(numberOfDecrease);
        size = Mathf.Max(size, 0);
        DBManager.instance.numberOfSkills[(int)skill] = size;
        if (size == 0)
            DeselectSkill(skill);
        DBManager.instance.UpdateNumberOfSkills();
    }

    public static void ClearAllSelectedSkills() 
    {
        //8
    }

    private static bool CheckSelectedFirstTwo()
    {
        if (DBManager.instance.selectedSkills[0] == 1 && DBManager.instance.selectedSkills[1] == 1)
            return true;
        return false;
    }
}

public enum Skills
{
    BiggerBall,
    SmallGoal,
    BiggerGloves,
    SlowBall,
    x2Score,
    ShowTarget
}