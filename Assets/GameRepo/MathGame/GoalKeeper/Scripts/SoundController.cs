using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

public class SoundController : MonoBehaviour
{
    public static SoundController Instance;

    [SerializeField] private AudioSource MainMenu;
    [SerializeField] private AudioSource ButtonClick;
    [SerializeField] private AudioSource SoccerBallKick;
    [SerializeField] private AudioSource SoccerBallHit;
    [SerializeField] private AudioSource Ambient;
    [SerializeField] private AudioSource Confirm;
    [SerializeField] private AudioSource SaveBall;
    [SerializeField] private AudioSource BallWind;
    [SerializeField] private AudioSource EndGame;
    [SerializeField] private AudioSource Whistle;

    [SerializeField] private List<AudioSource> ReactSave;
    [SerializeField] private List<AudioSource> ReactSad;
    [SerializeField] private List<AudioSource> EndGameSpeak;
    [SerializeField] private List<AudioSource> CountDownTimerSounds;

    [SerializeField] private TextMeshProUGUI MusicVolumePercentage;
    [SerializeField] private TextMeshProUGUI SFXVolumePercentage;
    [SerializeField] private TextMeshProUGUI AmbientVolumePercentage;
    [SerializeField] private Slider MusicSlider;
    [SerializeField] private Slider SFXSlider;
    [SerializeField] private Slider AmbientSlider;

    private float MusicVolumeScaler = .5f;
    private float sfxVolumeScaler = 1.0f;
    private float AmbientVolumeScaler = 1.0f;
    private float defaultMusicVolume, defaultSFXVolume, defaultAmbientVolume;
    private float LastMusicVolumeScaler, LastSFXVolumeScaler, LastAmbientVolumeScaler, GameSoundScaler = 1f;

    void Awake()
    {
        Instance = this;
        defaultMusicVolume = MainMenu.volume;
        defaultAmbientVolume = 1f;
        defaultSFXVolume = SoccerBallKick.volume;

        if (PlayerPrefs.HasKey("MusicVolumeScaler")) MusicVolumeScaler = PlayerPrefs.GetFloat("MusicVolumeScaler");
        if (PlayerPrefs.HasKey("SFXVolumeScaler")) sfxVolumeScaler = PlayerPrefs.GetFloat("SFXVolumeScaler");
        if (PlayerPrefs.HasKey("AmbientVolumeScaler")) AmbientVolumeScaler = PlayerPrefs.GetFloat("AmbientVolumeScaler");


        MainMenu.volume = defaultMusicVolume * MusicVolumeScaler;

        ButtonClick.volume = defaultSFXVolume * sfxVolumeScaler;
        SoccerBallKick.volume = defaultSFXVolume * sfxVolumeScaler;
        SoccerBallHit.volume = defaultSFXVolume * sfxVolumeScaler;
    }

    public void FadeOutMainMenuSound()
    {
        MainMenu.DOFade(0f, 4f);
    }
    public void FadeInMainMenuSound()
    {
        MainMenu.DOFade(defaultMusicVolume * MusicVolumeScaler, 4f);
    }
    public void PlayButtonClick()
    {
        ButtonClick.volume = defaultSFXVolume * sfxVolumeScaler;
        ButtonClick.Play();
    }
    public void PlayConfirm()
    {
        Confirm.volume = defaultSFXVolume * sfxVolumeScaler;
        Confirm.Play();
    }
    public void PlaySoccerBallKick()
    {
        SoccerBallKick.volume = defaultSFXVolume * sfxVolumeScaler * GameSoundScaler;
        SoccerBallKick.Play();
    }
    public void PlaySoccerBallHit(float Volume)
    {
        SoccerBallHit.volume = Volume * sfxVolumeScaler * defaultSFXVolume * GameSoundScaler;
        SoccerBallHit.Play();
    }
    public void PlayCountDownSound(int index)
    {
        CountDownTimerSounds[index].volume = defaultSFXVolume * sfxVolumeScaler * GameSoundScaler;
        CountDownTimerSounds[index].Play();
    }
    public void PlaySaveBall()
    {
        SaveBall.volume = defaultSFXVolume * sfxVolumeScaler * GameSoundScaler;
        SaveBall.Play();
    }
    public void PlayWhistle()
    {
        Whistle.volume = defaultSFXVolume * sfxVolumeScaler * GameSoundScaler;
        Whistle.Play();
    }
    public void PlayBallWind(float Pan)
    {
        BallWind.volume = defaultSFXVolume * sfxVolumeScaler * GameSoundScaler;
        BallWind.panStereo = Pan;
        BallWind.Play();
    }
    public void PlayReactSave()
    {
        int isPlay = UnityEngine.Random.Range(0, 3);
        if (isPlay == 0) return;

        AudioSource rand = ReactSave[UnityEngine.Random.Range(0, ReactSave.Count)];

        rand.volume = defaultAmbientVolume * AmbientVolumeScaler;
        rand.Play();
    }
    public void PlayReactSad()
    {
        int isPlay = UnityEngine.Random.Range(0, 3);
        if (isPlay == 0) return;

        AudioSource rand = ReactSad[UnityEngine.Random.Range(0, ReactSad.Count)];

        rand.volume = defaultAmbientVolume * AmbientVolumeScaler;
        rand.Play();
    }
    public void PlayEndGame()
    {
        EndGame.volume = defaultAmbientVolume * AmbientVolumeScaler;
        EndGame.Play();
    }
    public void PlayEndGameSpeak()
    {
        AudioSource rand = EndGameSpeak[UnityEngine.Random.Range(0, EndGameSpeak.Count)];
        rand.volume = defaultSFXVolume * sfxVolumeScaler * GameSoundScaler;
        rand.Play();
    }
    public void KillCountDownSounds()
    {
        for(int i = 0; i < 3; i++)
        {
            CountDownTimerSounds[i].Stop();
        }
    }
    public void ReduceSFX()
    {
        GameSoundScaler = .3f;
    }
    public void IncreaseSFX()
    {
        GameSoundScaler = 1f;
    }
    public void FadeInAmbient()
    {
        Ambient.DOFade(defaultAmbientVolume * AmbientVolumeScaler, 1f);
    }
    public void FadeOutAmbient()
    {
        Ambient.DOFade(0f, 1f);
    }
    public void SetMusicVolumeScaler(float Value)
    {
        MusicVolumeScaler = Value;
        LastMusicVolumeScaler = Value;
        MusicVolumePercentage.SetText(Mathf.FloorToInt(Value * 100f).ToString());
        MainMenu.volume = defaultMusicVolume * MusicVolumeScaler;
    }
    public void SetSFXVolumeScaler(float Value)
    {
        sfxVolumeScaler = Value;
        LastSFXVolumeScaler = Value;
        SFXVolumePercentage.SetText(Mathf.FloorToInt(Value * 100f).ToString());
    }
    public void SetAmbientVolumeScaler(float Value)
    {
        AmbientVolumeScaler = Value;
        LastAmbientVolumeScaler = Value;
        AmbientVolumePercentage.SetText(Mathf.FloorToInt(Value * 100f).ToString());
    }
    public void SaveMusicVolumeScaler()
    {
        PlayerPrefs.SetFloat("MusicVolumeScaler", LastMusicVolumeScaler);
    }
    public void SaveSFXVolumeScaler()
    {
        PlayerPrefs.SetFloat("SFXVolumeScaler", LastSFXVolumeScaler);
    }
    public void SaveAmbientVolumeScaler()
    {
        PlayerPrefs.SetFloat("AmbientVolumeScaler", LastAmbientVolumeScaler);
    }
    public void GetSliderValues()
    {
        if (PlayerPrefs.HasKey("MusicVolumeScaler")) MusicVolumeScaler = PlayerPrefs.GetFloat("MusicVolumeScaler");
        if (PlayerPrefs.HasKey("SFXVolumeScaler")) sfxVolumeScaler = PlayerPrefs.GetFloat("SFXVolumeScaler");
        if (PlayerPrefs.HasKey("AmbientVolumeScaler")) AmbientVolumeScaler = PlayerPrefs.GetFloat("AmbientVolumeScaler");

        MusicSlider.value = MusicVolumeScaler;
        SFXSlider.value = sfxVolumeScaler;
        AmbientSlider.value = AmbientVolumeScaler;
        MusicVolumePercentage.SetText(Mathf.FloorToInt(MusicVolumeScaler * 100f).ToString());
        SFXVolumePercentage.SetText(Mathf.FloorToInt(sfxVolumeScaler * 100f).ToString());
        AmbientVolumePercentage.SetText(Mathf.FloorToInt(AmbientVolumeScaler * 100f).ToString());
    }
}
