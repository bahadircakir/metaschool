using System;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

public class EndUIController : MonoBehaviour
{
    public MainMenuController mainMenuController;
    public GameObject Game;
    public GameObject MainMenu;
    public GameObject GameUI;
    public SoundController soundController;
    public Transform CharacterEndgamePos;
    public TMP_Text scoreText;
    public TMP_Text gameStatisticsText; // ex: 17/20 balls saved

    private void OnEnable()
    {
        scoreText.text = String.Concat("Score: ", ChallengeController.instance.currentScore.ToString());
        gameStatisticsText.text = ChallengeController.instance.savedBallsCount + "/" +
                                  ChallengeController.instance.totalBallsCount + " balls saved";
        MathGameController.instance.PauseGame();
        GameUI.SetActive(false);
        soundController.FadeInMainMenuSound();
        soundController.FadeOutAmbient();
        SoundController.Instance.PlayEndGame();
        SoundController.Instance.PlayEndGameSpeak();
        SoundController.Instance.PlayWhistle();
    }
    
    public void BackToMainMenu()
    {
        var gameObjects = GameObject.FindGameObjectsWithTag("Hand");
        foreach (var hand in gameObjects)
        {
            Destroy(hand);
        }
        gameObjects = GameObject.FindGameObjectsWithTag("HandShadow");
        foreach (var shadow in gameObjects)
        {
            Destroy(shadow);
        }
        
        MathGameController.instance.ResetScoreAndTimer();
        GameUI.SetActive(false);

        GameplayMode.Set(GamePlayModes.None);

        GetComponent<RectTransform>().DOScale(0f, 0.5f).OnComplete(() =>
        {
            GetComponent<RectTransform>().localScale = Vector3.one;
            gameObject.SetActive(false);
            MainMenu.SetActive(true);
            Invoke(nameof(Invoke_DisableGame), 2f);
        });
    }
    
    private void Invoke_DisableGame()
    {
        Game.SetActive(false);
    }

    private void ContinueGame()
    {
        Invoke(nameof(ContinueGameInvoke),1f);
    }

    public void ContinueGameInvoke()
    {
        gameObject.SetActive(false);
        mainMenuController.ExtraTime();
    }
}