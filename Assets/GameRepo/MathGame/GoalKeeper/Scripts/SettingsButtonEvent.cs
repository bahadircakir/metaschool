using UnityEngine;

public class SettingsButtonEvent : MonoBehaviour
{
    public CountDownTimer countDownTimer;

    public void OpenMainMenu(GameObject mainMenu)
    {
        countDownTimer.Kill();
        mainMenu.SetActive(true);
    }
}
