using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillActionTutorial : MonoBehaviour
{
    public static SkillActionTutorial instance;
    public bool TestComplete = false;
    private bool Lock = false;

    [Space(20)]

    public GameObject Description;
    public GameObject Done;
    public SoundController soundController;

    private void Awake()
    {
        instance = this;
    }

    private void OnEnable()
    {
        if (SkillDatabase.GetSelectedSkills().Count == 1)
        {
            GetComponent<RectTransform>().anchoredPosition = new Vector3(GetComponent<RectTransform>().anchoredPosition.x, -245f, 0f);
        }
        Lock = false;
        TestComplete = false;
        Description.SetActive(true);
        Done.SetActive(false);
    }


    public void SkillSelectionCompleted()
    {
        Done.SetActive(true);
        Description.SetActive(false);
        soundController.PlayConfirm();
        Invoke("Invoke_Disable", 2f);
        Invoke("Invoke_StartGame", 3f);
    }
    private void Invoke_Disable()
    {
        
        gameObject.SetActive(false);
    }
    private void Invoke_StartGame()
    {
        MathGameController.instance.StartGame();
    }



    private void Update()
    {
        if(TestComplete && !Lock)
        {
            Lock = true;
            SkillSelectionCompleted();
        }
    }
    

}
