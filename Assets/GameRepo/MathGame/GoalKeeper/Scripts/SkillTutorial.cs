using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SkillTutorial : MonoBehaviour
{

    public GameObject State1;
    public GameObject State2;

    [Space(20f)]

    public bool first2SkillsSelected = false;
    public bool secondSkillDeselected = false;
    public bool thirdSkillSelected = false;
    public static SkillTutorial instance;

    private void Awake()
    {
        instance = this;
    }

    private void OnEnable()
    {
        DisableAllStates();
        State1.SetActive(true);
    }


    #region Sirasiyla calisacak methodlar
    public void First2SkillsSelected()
    {
        DisableAllStates();
        State2.SetActive(true);

        first2SkillsSelected = false;
    }
    public void SecondSkillDeselected()
    {
        State2.GetComponent<Animator>().SetBool("Continue", true);

        secondSkillDeselected = false;
    }
    public void ThirdSkillSelected()
    {
        State2.GetComponent<Animator>().SetBool("Continue 2", true);

        thirdSkillSelected = false;
        DBManager.instance.isFirstTimeUsing = 0;
        DBManager.instance.UpdateIsFirstTimeUsing();
    }
    #endregion



    #region diger

    private void Update()
    {
        if (first2SkillsSelected) First2SkillsSelected();
        if (secondSkillDeselected) SecondSkillDeselected();
        if (thirdSkillSelected) ThirdSkillSelected();
    }
    public void ClickNext()
    {
        State2.GetComponent<Animator>().SetBool("Next", true);
    }
    public void ExitTutorial()
    {
        gameObject.SetActive(false);
    }

    private void DisableAllStates()
    {
        State1.SetActive(false);
        State2.SetActive(false);
    }


    #endregion
}