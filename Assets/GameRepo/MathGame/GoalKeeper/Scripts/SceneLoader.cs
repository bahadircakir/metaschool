using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneLoader : MonoBehaviour
{
    public GameObject LoadingCanvas;

    void Start()
    {
        DontDestroyOnLoad(LoadingCanvas);
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        var operation = SceneManager.LoadSceneAsync("GoalKeeper");
        operation.allowSceneActivation = false;
        yield return new WaitForSeconds(3.5f);
        while (!operation.isDone) {
            if (operation.progress >= 0.9f)
            {
                operation.allowSceneActivation = true;
            }
            yield return null;
        }
    }
}
