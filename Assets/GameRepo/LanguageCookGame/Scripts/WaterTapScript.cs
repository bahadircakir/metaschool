using UnityEngine;

public class WaterTapScript : MonoBehaviour {
    [SerializeField]
    public ParticleSystem sinkWater;

    private void Awake(){
        sinkWater = GetComponent<ParticleSystem>();
        var emission = sinkWater.emission;
        emission.enabled = false;
    }

    private void OnTriggerEnter(Collider other){
        var emission = sinkWater.emission;
        emission.enabled = true;
    }

    private void OnTriggerExit(Collider other){
        var emission = sinkWater.emission;
        emission.enabled = false;
    }

    private void OnCollisionEnter(Collision other){
        var emission = sinkWater.emission;
        emission.enabled = true;
    }

    private void OnCollisionExit(Collision other){
        var emission = sinkWater.emission;
        emission.enabled = false;
    }
}